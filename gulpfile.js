/**
 * Gulp Packages.
 */

// General.
const gulp = require("gulp");
const path = require("path");
const fs = require("fs");
const del = require("del");
const lazypipe = require("lazypipe");
const plumber = require("gulp-plumber");
const flatten = require("gulp-flatten");
const tap = require("gulp-tap");
const rename = require("gulp-rename");
const header = require("gulp-header");
const footer = require("gulp-footer");
const fileinclude = require("gulp-file-include");
const ignore = require("gulp-ignore");
const zip = require("gulp-zip");
const browser = require("browser-sync").create();
const package = require("./package.json");

// Scripts and tests.
const jshint = require("gulp-jshint");
const stylish = require("jshint-stylish");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const wpPot = require("gulp-wp-pot");

// Styles.
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const prefix = require("gulp-autoprefixer");
const minify = require("gulp-cssnano");

// Media.
const imagemin = require("gulp-imagemin");
const svgmin = require("gulp-svgmin");
const svgstore = require("gulp-svgstore");
const cheerio = require("gulp-cheerio");
const compress_img = require("compress-images");

/**
 * Config to project.
 */

const config = {
	proxy: "http://localhost:3000",
	port: 3000,
};

/**
 * Paths to project folders.
 */

const paths = {
	scripts: {
		input: "src/js/*",
		output: "assets/js/",
	},
	styles: {
		input: "src/scss/**/*.{scss,sass}",
		output: "assets/css/",
		includePaths: [
			path.resolve(__dirname, "./"),
			path.resolve(__dirname, "./assets/css/"),
			path.resolve(__dirname, "./node_modules/"),
		],
	},
	images: {
		wp: "../../uploads/**/**/*.{png,PNG,jpg,JPG,jpeg,JPEG,gif,GIF,svg,SVG}",
		wpo: "../../optimg/",
		wpo2: "../../optimg2/",
		input: "src/img/**/*",
		output: "assets/img/",
	},
	fonts: {
		input: "src/fonts/**/*",
		output: "assets/fonts/",
	},
	icons: {
		input: "src/icons/*.svg",
		output: "assets/icons/",
	},
	dist: {
		input: "**",
		ignore: [
			"dist/**",
			"dist",
			"*.zip",
			"*.sublime-project",
			"*.sublime-workspace",
			"node_modules/**",
			"node_modules",
			"bower_components/**",
			"bower_components",
			"src/**",
			"src",
			"bower.json",
			"gulpfile.js",
			"package.json",
			"README.md",
			"yarn.lock",
			".vscode",
			".remote*",
		],
		output: "dist/",
	},
};

/**
 * Template for banner to add to file headers.
 */

const banner = {
	full:
		"/*!\n" +
		" * <%= package.name %> v<%= package.version %> <<%= package.homepage %>>\n" +
		" * <%= package.title %> : <%= package.description %>\n" +
		" * (c) " +
		new Date().getFullYear() +
		" <%= package.author.name %> <<%= package.author.url %>>\n" +
		" * MIT License\n" +
		" * <%= package.repository.url %>\n" +
		" */\n\n",
	min:
		"/*!\n" +
		" * <%= package.name %> v<%= package.version %> <<%= package.homepage %>>\n" +
		" * <%= package.title %> : <%= package.description %>\n" +
		" * (c) " +
		new Date().getFullYear() +
		" <%= package.author.name %> <<%= package.author.url %>>\n" +
		" * MIT License\n" +
		" * <%= package.repository.url %>\n" +
		" */\n",
};

/**
 * Gulp Taks.
 */

// Lint, minify, and concatenate scripts.
gulp.task("build:scripts", function () {
	var jsTasks = lazypipe()
		.pipe(header, banner.full, { package: package })
		.pipe(gulp.dest, paths.scripts.output)
		.pipe(rename, { suffix: ".min" })
		.pipe(uglify)
		.pipe(header, banner.min, { package: package })
		.pipe(gulp.dest, paths.scripts.output);

	return gulp
		.src(paths.scripts.input)
		.pipe(plumber())
		.pipe(
			fileinclude({
				prefix: "//=",
				basepath: "@file",
			})
		)
		.pipe(
			tap(function (file, t) {
				if (file.isDirectory()) {
					var name = file.relative + ".js";
					return gulp
						.src(file.path + "/*.js")
						.pipe(concat(name))
						.pipe(jsTasks());
				}
			})
		)
		.pipe(jsTasks());
});

// Process, lint, and minify Sass files.
gulp.task("build:styles", function () {
	return (
		gulp
			.src(paths.styles.input)
			.pipe(sourcemaps.init())
			.pipe(plumber())
			.pipe(
				sass({
					outputStyle: "expanded",
					sourceComments: false,
					includePaths: paths.styles.includePaths,
				})
			)
			.pipe(flatten())
			.pipe(
				prefix({
					cascade: true,
					remove: true,
				})
			)
			.pipe(gulp.dest(paths.styles.output))
			.pipe(header(banner.full, { package: package }))
			.pipe(rename({ suffix: ".min" }))
			.pipe(
				minify({
					discardComments: {
						removeAll: true,
					},
				})
			)
			.pipe(header(banner.min, { package: package }))
			// .pipe(sourcemaps.write("./maps"))
			.pipe(gulp.dest(paths.styles.output))
			.pipe(browser.stream())
	);
});

// Copy image files into output folder.
gulp.task("build:images", function () {
	compress_img(
		paths.images.input,
		paths.images.output,
		{ compress_force: false, statistic: true, autoupdate: true },
		false,
		{ jpg: { engine: "mozjpeg", command: ["-quality", "70"] } },
		{ png: { engine: "pngquant", command: ["--quality=20-50"] } },
		{ svg: { engine: "svgo", command: "--multipass" } },
		{
			gif: {
				engine: "gifsicle",
				command: ["--colors", "64", "--use-col=web"],
			},
		},
		function () {}
	);
});

gulp.task("wp:img", function () {
	compress_img(
		paths.images.wp,
		paths.images.wpo,
		{ compress_force: false, statistic: true, autoupdate: true },
		false,
		{ jpg: { engine: "mozjpeg", command: ["-quality", "70"] } },
		{ png: { engine: "pngquant", command: ["--quality=20-50"] } },
		{ svg: { engine: "svgo", command: "--multipass" } },
		{
			gif: {
				engine: "gifsicle",
				command: ["--colors", "64", "--use-col=web"],
			},
		},
		function () {}
	);
});

gulp.task("build:icons", function () {
	return gulp
		.src(paths.icons.input)
		.pipe(svgmin())
		.pipe(
			svgstore({
				fileName: "icons.svg",
				inlineSvg: true,
			})
		)
		.pipe(
			cheerio({
				run: function ($, file) {
					$("svg").addClass("d-none");
					$("[fill]").removeAttr("fill");
				},
				parserOptions: { xmlMode: true },
			})
		)
		.pipe(gulp.dest(paths.icons.output))
		.pipe(browser.stream());
});

// Copy fonts files into output folder.
gulp.task("build:fonts", function () {
	return gulp
		.src(paths.fonts.input)
		.pipe(plumber())
		.pipe(gulp.dest(paths.fonts.output));
});

// Gen POT file
gulp.task("pot", function () {
	return gulp
		.src(["*.php", "{inc,components}/**/*.php"])
		.pipe(
			wpPot({
				domain: "odin",
				package: "Odin",
			})
		)
		.pipe(gulp.dest("languages/lang.pot"));
});

// Lint scripts.
gulp.task("lint:scripts", function () {
	return gulp
		.src(paths.scripts.input)
		.pipe(plumber())
		.pipe(jshint())
		.pipe(jshint.reporter("jshint-stylish"));
});

// Remove pre-existing content from scripts output directory.
gulp.task("clean:scripts", function () {
	return del.sync(paths.scripts.output);
});

// Remove pre-existing content from styles output directory.
gulp.task("clean:styles", function () {
	return del.sync(paths.styles.output);
});

// Remove pre-existing content from images output directory.
gulp.task("clean:images", function () {
	return del.sync(paths.images.output);
});

// Remove pre-existing content from icons output directory.
gulp.task("clean:icons", function () {
	return del.sync(paths.icons.output);
});

// Remove pre-existing content from fonts output directory.
gulp.task("clean:fonts", function () {
	return del.sync(paths.fonts.output);
});

// Generate zip dist.
gulp.task("build:dist", function () {
	return gulp
		.src(paths.dist.input)
		.pipe(ignore.exclude(paths.dist.ignore))
		.pipe(plumber())
		.pipe(zip(package.name + "-v" + package.version + ".zip"))
		.pipe(gulp.dest(paths.dist.output));
});

// Starts a browser instance.
gulp.task("serve", function () {
	var files = [
		paths.styles.output + "*.css",
		paths.scripts.output + "*.js",
		paths.images.output + "*",
		paths.icons.output + "*.svg",
		"*.php",
		"{inc,template-parts,custom-parts,woocommerce}/**/*.php",
	];
	browser.init(files, {
		// logLevel: 'debug',
		// https: true,
		// https: {
		// 	key: "inc/keys/privkey.pem",
		// 	cert: "inc/keys/cert.pem",
		// },
		socket: {
			domain: config.proxy,
		},
		proxy: {
			target: config.proxy,
		},
		port: config.port,
		ui: {
			port: 3001,
		},
		open: false,
		notify: false,
		injectChanges: true,
	});
});

// Watch files for changes.
gulp.task("watch", function () {
	gulp.watch(paths.styles.input, ["styles"]);
	gulp.watch(paths.scripts.input, ["scripts", browser.reload]);
	gulp.watch(paths.icons.input, ["icons", browser.reload]);
	gulp.watch(paths.images.input, ["images"]);
	gulp.watch(paths.fonts.input, ["fonts", browser.reload]);
	gulp
		.watch(["*.php", "{inc,components}/**/*.php"])
		.on("change", browser.reload);
});

/**
 * Task Runners.
 */

// Compile all files (default).
gulp.task("default", ["pot", "scripts", "styles", "images", "fonts", "icons"]);

// Compile, init serve and watch files.
gulp.task("start", ["default", "serve", "watch"]);

// Generate dist.
gulp.task("dist", ["build:dist"]);

// Compile scripts files.
gulp.task("scripts", ["clean:scripts", "lint:scripts", "build:scripts"]);

// Compile styles files.
gulp.task("styles", ["clean:styles", "build:styles"]);

// Compile images files.
gulp.task("images", ["clean:images", "build:images"]);

// Compile icons files.
gulp.task("icons", ["clean:icons", "build:icons"]);

// Compile fonts files.
gulp.task("fonts", ["clean:fonts", "build:fonts"]);
