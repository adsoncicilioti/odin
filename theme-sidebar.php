<?php
/**
 * Template Name: Page With Sidebar
 *
 * The template for displaying pages with sidebar.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header();




// start of Content and Wrappers
get_template_part( 'components/content', 'start' ); 

// Start the Loop.
while ( have_posts() ) : the_post();

	// Include the page content template.
	get_template_part( 'components/content', get_post_format() );

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
endwhile;

// end of Content and Wrappers
get_template_part( 'components/content', 'end' ); 
