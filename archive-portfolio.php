<?php
/**
 * The template for displaying Portfolio Post typ.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header();




// start of Content and Wrappers
get_template_part( 'components/content', 'start' ); ?>

    <div class="entry-content">
  		<div class="bg-white ">
          	<div class="container">
				<?php
				global $wp_query;
				$paged = ( get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'portfolio',
					'showposts' => 12,
					'paged' => $paged
				);
				$temp = $wp_query;  // assign original query to temp variable for later use
				$wp_query = null;
				$wp_query = new WP_Query($args);

				?>
				<?php   if( $wp_query->have_posts() ) : ?>
				<div class="portfolio cf">
					<?php while( $wp_query->have_posts() ) : $wp_query->the_post(); 

					$thumb = '<img class="ease" src="' . get_bloginfo('template_directory') . '/assets/images/nothumb.jpg" alt="' .  __( 'No image','odin' ) .'"/>';
					$thumb = ( has_post_thumbnail() ) ?	odin_thumbnail( 300, 180, get_the_title(), true, 'ease' ) : $thumb;

					?>
					<a 	href="<?php the_permalink(); ?>"
						title="<?php the_title(); ?>"
						rel="bookmark" id="post-<?php the_ID(); ?>"
						<?php post_class('col-xs-6 col-md-4 p-0 portfolio-item relative'); ?>>
						<?php echo $thumb; ?>
					</a>
					<?php endwhile; ?>

				</div><!-- Portfolio -->
			<?php
			wp_reset_postdata();
			endif;
			?>
         	</div>
      	</div>
      	<?php odin_paging_nav(); ?>
  	</div><!-- .entry-content -->

<?php // end of Content and Wrappers
get_template_part( 'components/content', 'end' ); ?>
