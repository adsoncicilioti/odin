# Odin Theme Framework #

## Project Description ##

This is my personal fork of Odin, a basic theme developed by [WordPress Brasil Group](https://www.facebook.com/groups/wordpress.brasil), to help in the agile development of themes for WordPress.

Learn more about the original project:

* [Website](http://wpod.in/)
* [Odin Expo](http://expo.wpod.in/)
* [GitHub](https://github.com/wpbrasil/odin)
* [WordPress Brasil Group on Facebook](https://www.facebook.com/groups/wordpress.brasil)
