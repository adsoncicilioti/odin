<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main div element.
 *
 * @package Odin
 * @since 2.2.0
 */
?>


<footer id="footer" role="contentinfo" class="ease">


	<div class="container">

		<div class="cf py-5 d-flex align-items-center justify-content-center">

			<?php get_template_part( 'components/foot', 'logo' ); ?>
			<?php // get_template_part( 'components/foot', 'navmenu' ); ?>
			<?php // get_template_part( 'components/foot', 'socials' ); ?>
			<span >&copy; <?php echo date( 'Y' ); ?>
				<a class="d-none" href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a>
				- <?php esc_html_e( 'All rights reserved. Made by ', 'odin' ); ?>
				<a href="https://adsonagencia.com" title="<?php esc_html_e( 'Access the website of developer', 'odin' ); ?>" target="_blank">
					<img width="70" height="22" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/adson-ass.svg" alt="Adson^ Agência de Resultados">
				</a>
			</span>
		</div>

	</div><!-- .container -->
</footer><!-- #footer -->

<?php

/**
 * Call Modals here
 */
get_template_part( 'components/nav', 'sign' );
get_template_part( 'components/nav', 'search' );

?>

<?php
if ( file_exists( get_template_directory() . '/assets/icons/icons.svg' ) ) {
	require_once get_template_directory() . '/assets/icons/icons.svg';
}
?>

<?php wp_footer(); ?>
</body>
</html>
