<?php get_header(); ?>
<header class="page-header ">
<div class="container">
  <?php the_title( '<h1 class="card-title entry-title">', '</h1>' ); ?>
</div>
</header><!-- .page-header -->
<div class="container">
	<main id="content" class="p-0 <?php echo odin_classes_page_full(); ?>" tabindex="-1" role="main">
    <div id="post-<?php the_ID(); ?>" <?php post_class( 'no-radius card bg-white ' ); ?>>
      <?php if ( has_post_thumbnail() ) : ?>
          <?php echo odin_thumbnail( 840, 400, get_the_title(), true, 'card-img-top no-radius' );?>
      <?php endif;	?>
      <div class="card-block p-2">
        <div class="entry-content">

        <?php while(have_posts()): the_post(); ?>

          <?php the_content() ?>

        <?php endwhile; ?>

        </div><!-- .entry-content -->
      </div>

    </div><!-- #post-## -->
	</main><!-- #main -->
</div>

<?php
get_footer();
