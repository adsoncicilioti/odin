<?php

if ( ! function_exists( 'odin_theme_support' ) ) :
	/**
	 * Setup theme support.
	 *
	 * @link https://codex.wordpress.org/Function_Reference/add_theme_support
	 */
	function odin_theme_support() {

		// Add multiple languages support.
		load_theme_textdomain( 'odin', get_template_directory() . '/languages' );

		// Add post_thumbnails suport.
		add_theme_support( 'post-thumbnails' );

		// Add feed link support.
		add_theme_support( 'automatic-feed-links' );

		// Add support custom header support.
		add_theme_support(
			'custom-header',
			array(
				'width'           => 1200,
				'height'          => 400,
				'flex-width'      => true,
				'flex-height'     => true,
				'header-text'     => false,
				'default-image'   => '',
				'uploads'         => true,
				'header-selector' => '.site-title a',
			)
		);

		// Add support custom background support.
		add_theme_support(
			'custom-background',
			array(
				'default-color' => '',
				'default-image' => '',
			)
		);

		// Add custom editor style support.
		add_theme_support( 'editor-styles' );
		add_editor_style( trailingslashit( get_template_directory_uri() ) . 'assets/css/editor-style.min.css' );

		// Add infinite scroll support.
		add_theme_support(
			'infinite-scroll',
			array(
				'type'           => 'scroll',
				'footer_widgets' => false,
				'container'      => 'content',
				'wrapper'        => false,
				'render'         => false,
				'posts_per_page' => get_option( 'posts_per_page' ),
			)
		);

		// Add support for Post Formats.
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'gallery',
				'link',
				'image',
				'quote',
				'status',
				'video',
				'audio',
				'chat',
			)
		);

		// Add the excerpt on pages.
		add_post_type_support( 'page', 'excerpt' );

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// Add custom logo support.
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 140,
				'width'       => 140,
				'flex-height' => true,
				'flex-width'  => true,
				'header-text' => array( 'site-title', 'site-description' ),
			)
		);

	} /* end theme support */
endif;

add_action( 'after_setup_theme', 'odin_theme_support' );

if ( ! function_exists( 'odin_content_width' ) ) :
	/**
	 * Sets the content width in pixels, based on the theme's design and stylesheet.
	 *
	 * Priority 0 to make it available to lower priority callbacks.
	 *
	 * @global int $content_width
	 */
	function odin_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'odin_content_width', 860 );
	}
endif;

add_action( 'after_setup_theme', 'odin_content_width', 0 );

/**
 *
 * SVG support ********************
 *
 * Return the accepted value for SVG mime-types in compliance with the RFC 3023.
 * RFC 3023: https://www.ietf.org/rfc/rfc3023.txt 8.19, A.1, A.2, A.3, A.5, and A.7
 * Expects to interface with https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 */
function allow_svg_uploads( $existing_mime_types = array() ) {
	return $existing_mime_types + array( 'svg' => 'image/svg+xml' );
}

/**
 * This is a decent way of grabbing the dimensions of SVG files.
 * Depends on http://php.net/manual/en/function.simplexml-load-file.php
 * I believe this to be a reasonable dependency and should be common enough to
 * not cause problems.
 */
function get_dimensions( $svg ) {
	$svg        = simplexml_load_file( $svg );
	$attributes = $svg->attributes();
	$width      = (string) $attributes->width;
	$height     = (string) $attributes->height;

	return (object) array(
		'width'  => $width,
		'height' => $height,
	);
}

/**
 * Browsers may or may not show SVG files properly without a height/width.
 * WordPress specifically defines width/height as "0" if it cannot figure it out.
 * Thus the below is needed.
 *
 * Consider this the "server side" fix for dimensions.
 * Which is needed for the Media Grid within the Administration area.
 */
function set_dimensions( $response, $attachment, $meta ) {
	if ( 'image/svg+xml' === $response['mime'] && empty( $response['sizes'] ) ) {
		$svg_file_path = get_attached_file( $attachment->ID );
		$dimensions    = get_dimensions( $svg_file_path );

		$response['sizes'] = array(
			'full' => array(
				'url'         => $response['url'],
				'width'       => $dimensions->width,
				'height'      => $dimensions->height,
				'orientation' => $dimensions->width > $dimensions->height ? 'landscape' : 'portrait',
			),
		);
	}

	return $response;
}

/**
 * Browsers may or may not show SVG files properly without a height/width.
 * WordPress specifically defines width/height as "0" if it cannot figure it out.
 * Thus the below is needed.
 *
 * Consider this the "client side" fix for dimensions. But only for the Administration.
 *
 * WordPress requires inline administration styles to be wrapped in an actionable function.
 * These styles specifically address the Media Listing styling and Featured Image
 * styling so that the images show up in the Administration area.
 */
function administration_styles() {
	// Media Listing Fix
	wp_add_inline_style( 'wp-admin', ".media .media-icon img[src$='.svg'] { width: auto; height: auto; }" );
	// Featured Image Fix
	wp_add_inline_style( 'wp-admin', "#postimagediv .inside img[src$='.svg'] { width: 100%; height: auto; }" );
}

/**
 * Browsers may or may not show SVG files properly without a height/width.
 * WordPress specifically defines width/height as "0" if it cannot figure it out.
 * Thus the below is needed.
 *
 * Consider this the "client side" fix for dimensions. But only for the End User.
 */
function public_styles() {
	// Featured Image Fix
	echo "<style>.post-thumbnail img[src$='.svg'] { width: 100%; height: auto; }</style>";
}

/**
 * Restores the ability to upload non-image files in WordPress 4.7.1 and 4.7.2.
 * Related Trac Ticket: https://core.trac.wordpress.org/ticket/39550
 * Credit: @sergeybiryukov
 * @TODO: Remove the plugin once WordPress 4.7.3 is available!
 */
function disable_real_mime_check( $data, $file, $filename, $mimes ) {
	$wp_filetype = wp_check_filetype( $filename, $mimes );

	$ext             = $wp_filetype['ext'];
	$type            = $wp_filetype['type'];
	$proper_filename = $data['proper_filename'];

	return compact( 'ext', 'type', 'proper_filename' );
}

if ( $wp_version < '4.7.3' ) {
	add_filter( 'wp_check_filetype_and_ext', 'disable_real_mime_check', 10, 4 );
}
add_filter( 'upload_mimes', 'allow_svg_uploads' );
add_filter( 'wp_prepare_attachment_for_js', 'set_dimensions', 10, 3 );
add_action( 'admin_enqueue_scripts', 'administration_styles' );
add_action( 'wp_head', 'public_styles' );
// END SVG support

/**
 * Gutenberg Support
 */

/**
 * Enqueue WordPress theme styles within Gutenberg.
 */
function odin_gutenberg_styles() {
	// Load the theme styles within Gutenberg.
	wp_enqueue_style( 'odin-gutenberg', get_theme_file_uri( '/assets/css/gutenberg-editor.min.css' ), false, '@@pkg.version', 'all' );
}
add_action( 'enqueue_block_editor_assets', 'odin_gutenberg_styles' );

/**
 * Array suports
 */
function setup_gutenberg_support() {
	add_theme_support( 'wide-images' );
	add_theme_support( 'align-wide' );
	add_theme_support(
		// Make specific theme colors available in the editor.
		'editor-color-palette',
		array(
			array(
				'name'  => __( 'White', 'odin' ),
				'slug'  => 'white',
				'color' => '#fff',
			),
			array(
				'name'  => __( 'Light gray', 'odin' ),
				'slug'  => 'gray-200',
				'color' => '#eeeff2',
			),
			array(
				'name'  => __( 'Gray', 'odin' ),
				'slug'  => 'gray-600',
				'color' => '#6a6d75',
			),
			array(
				'name'  => __( 'Dark gray', 'odin' ),
				'slug'  => 'gray-900',
				'color' => '#282a2e',
			),
			array(
				'name'  => __( 'Black', 'odin' ),
				'slug'  => 'black',
				'color' => '#000',
			),
			array(
				'name'  => __( 'Navy', 'odin' ),
				'slug'  => 'navy',
				'color' => '#000d4c',
			),
			array(
				'name'  => __( 'Dark Blue', 'odin' ),
				'slug'  => 'dark-blue',
				'color' => '#0f2799',
			),
			array(
				'name'  => __( 'Blue', 'odin' ),
				'slug'  => 'blue',
				'color' => '#4875fa',
			),
			array(
				'name'  => __( 'Indigo', 'odin' ),
				'slug'  => 'indigo',
				'color' => '#6610f2',
			),
			array(
				'name'  => __( 'Purple', 'odin' ),
				'slug'  => 'purple',
				'color' => '#6f42c1',
			),
			array(
				'name'  => __( 'Pink', 'odin' ),
				'slug'  => 'pink',
				'color' => '#e83e8c',
			),
			array(
				'name'  => __( 'Red', 'odin' ),
				'slug'  => 'red',
				'color' => '#d6102b',
			),
			array(
				'name'  => __( 'Orange', 'odin' ),
				'slug'  => 'orange',
				'color' => '#fd7e14',
			),
			array(
				'name'  => __( 'Yellow', 'odin' ),
				'slug'  => 'yellow',
				'color' => '#fcc953',
			),
			array(
				'name'  => __( 'Green', 'odin' ),
				'slug'  => 'green',
				'color' => '#2ea259',
			),
			array(
				'name'  => __( 'Teal', 'odin' ),
				'slug'  => 'teal',
				'color' => '#20c997',
			),
			array(
				'name'  => __( 'Cyan', 'odin' ),
				'slug'  => 'cyan',
				'color' => '#17a2b8',
			),
		)
	);
}

add_action( 'after_setup_theme', 'setup_gutenberg_support' );
