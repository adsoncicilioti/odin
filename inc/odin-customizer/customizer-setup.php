<?php
// Odin Customizer Setup

Kirki::add_config(
	'theme_config_id',
	array(
		'capability'  => 'edit_theme_options',
		'option_type' => 'theme_mod',
	)
);


// ======== PANEL (Parent Section) ========== //

Kirki::add_panel(
	'bizdata_panel',
	array(
		'priority'    => 60,
		'title'       => __( 'Business data', 'odin' ),
		'description' => __( 'Data of your business for this website.', 'odin' ),
	)
);

// function callback_theme_home() {
// 	return is_front_page();
// }

// Kirki::add_panel(
// 	'home_page_panel',
// 	array(
// 		'priority'        => 60,
// 		'title'           => __( 'Home Page', 'odin' ),
// 		'description'     => __( 'Home Page customization settings', 'odin' ),
// 		'active_callback' => 'callback_theme_home',
// 	)
// );



// function callback_theme_home() {
// 	return is_page_template( 'theme-home.php' );
// }
// Kirki::add_panel(
// 	'home_panel',
// 	array(
// 		'priority'        => 50,
// 		'title'           => __( 'Homepage', 'odin' ),
// 		'description'     => __( 'Homepage customization settings', 'odin' ),
// 		'active_callback' => 'callback_theme_home',
// 	)
// );

// =========== SECTIONS ========= //
// Kirki::add_section(
// 	'odin_home_slides',
// 	array(
// 		'title'       => __( 'Home Slides', 'odin' ),
// 		'description' => __( 'Slides for build home page', 'odin' ),
// 		'panel'       => 'home_panel', // Not typically needed.
// 		'priority'    => 50,
// 		'capability'  => 'edit_theme_options',
// 	)
// );

// Kirki::add_section(
// 	'odin_home_blocks',
// 	array(
// 		'title'       => __( 'Home Blocks', 'odin' ),
// 		'description' => __( 'Blocks for build home page', 'odin' ),
// 		'panel'       => 'home_panel', // Not typically needed.
// 		'priority'    => 50,
// 		'capability'  => 'edit_theme_options',
// 	)
// );

Kirki::add_section(
	'odin_biz_data',
	array(
		'title'       => __( 'Business data', 'odin' ),
		'description' => __( 'Basic informations about your Brand', 'odin' ),
		'panel'       => 'bizdata_panel', // Not typically needed.
		'priority'    => 50,
		'capability'  => 'edit_theme_options',
	)
);

// Kirki::add_section(
// 	'odin_hero_home',
// 	array(
// 		'title'       => __( 'Full Banner', 'odin' ),
// 		'description' => __( 'Define the banner section for Home Page', 'odin' ),
// 		'panel'       => 'home_page_panel', // Not typically needed.
// 		'priority'    => 50,
// 		'capability'  => 'edit_theme_options',
// 	)
// );

Kirki::add_section(
	'odin_social_links',
	array(
		'title'       => __( 'Social Links', 'odin' ),
		'description' => __( 'Social Profiles and Page from your Brand', 'odin' ),
		'panel'       => 'bizdata_panel', // Not typically needed.
		'priority'    => 50,
		'capability'  => 'edit_theme_options',
	)
);


// ============ FIELDS ============ //


// Logo footer
Kirki::add_field(
	'Kirki',
	array(
		'type'     => 'image',
		'settings' => 'odin_footer_logo',
		'label'    => __( 'Logo for footer section', 'odin' ),
		'section'  => 'title_tagline',
		'default'  => '',
	)
);

// Address
Kirki::add_field(
	'Kirki',
	array(
		'type'        => 'text',
		'settings'    => 'street_address',
		'label'       => __( 'Address', 'odin' ),
		'section'     => 'odin_biz_data',
		'input_attrs' => array(
			'placeholder' => __( 'Eg: Street one two, 123, Center', 'odin' ),
		),
	)
);

// City & State
Kirki::add_field(
	'Kirki',
	array(
		'type'        => 'text',
		'settings'    => 'city_state',
		'label'       => __( 'City, State', 'odin' ),
		'section'     => 'odin_biz_data',
		'input_attrs' => array(
			'placeholder' => __( 'Eg: São Paulo, SP', 'odin' ),
		),
	)
);

// ZIP Code
Kirki::add_field(
	'Kirki',
	array(
		'type'        => 'text',
		'settings'    => 'cep',
		'label'       => __( 'ZIP', 'odin' ),
		'section'     => 'odin_biz_data',
		'input_attrs' => array(
			'placeholder' => __( 'Eg: 12345-678', 'odin' ),
		),
	)
);

// Country
Kirki::add_field(
	'Kirki',
	array(
		'type'        => 'text',
		'settings'    => 'country',
		'label'       => __( 'Country', 'odin' ),
		'section'     => 'odin_biz_data',
		'input_attrs' => array(
			'placeholder' => __( 'Eg: Brazil', 'odin' ),
		),
	)
);

// Contact Email
Kirki::add_field(
	'Kirki',
	array(
		'type'     => 'email',
		'settings' => 'contact_mail',
		'label'    => __( 'E-mail', 'odin' ),
		'section'  => 'odin_biz_data',
	)
);

// Phone
Kirki::add_field(
	'Kirki',
	array(
		'type'      => 'repeater',
		'label'     => __( 'Phone(s)', 'odin' ),
		'section'   => 'odin_biz_data',
		'row_label' => array(
			'type'  => 'field',
			'value' => __( 'Number', 'odin' ),
			'field' => 'phone_num',
		),
		'settings'  => 'contact_phone',
		'default'   => '',
		'fields'    => array(
			'phone_num' => array(
				'type'  => 'tel',
				'label' => __( 'Phone number:', 'odin' ),
			),
		),
	)
);

// Social links
Kirki::add_field(
	'Kirki',
	array(
		'type'      => 'repeater',
		'label'     => __( 'Social Profiles', 'odin' ),
		'section'   => 'odin_social_links',
		'row_label' => array(
			'type'  => 'field',
			'value' => __( 'Profile', 'odin' ),
			'field' => 'social_type',
		),
		'settings'  => 'social_links',
		'fields'    => array(
			'social_type' => array(
				'type'    => 'radio-image',
				'label'   => __( 'What social network?', 'odin' ),
				'default' => '',
				'choices' => array(
					'facebook'    => get_template_directory_uri() . '/assets/img/icons/facebook.png',
					'twitter'     => get_template_directory_uri() . '/assets/img/icons/twitter.png',
					'google-plus' => get_template_directory_uri() . '/assets/img/icons/google-plus.png',
					'instagram'   => get_template_directory_uri() . '/assets/img/icons/instagram.png',
					'linkedin'    => get_template_directory_uri() . '/assets/img/icons/linkedin.png',
					'whatsapp'    => get_template_directory_uri() . '/assets/img/icons/whatsapp.png',
					'pinterest'   => get_template_directory_uri() . '/assets/img/icons/pinterest.png',
					'youtube'     => get_template_directory_uri() . '/assets/img/icons/youtube.png',
					'vimeo'       => get_template_directory_uri() . '/assets/img/icons/vimeo.png',
				),
			),
			'social_user' => array(
				'type'        => 'text',
				'label'       => __( 'Profile URL or Username', 'odin' ),
				'description' => __( 'For Whatsapp use full number. Eg: +12(34)5555-6789', 'odin' ),
			),
		),
	)
);

/**
 * HOME HERO
 */

// Kirki::add_field(
// 	'Kirki',
// 	array(
// 		'type'     => 'image',
// 		'settings' => 'hero_img',
// 		'label'    => __( 'Banner Image', 'odin' ),
// 		'section'  => 'odin_hero_home',
// 	)
// );

// Kirki::add_field(
// 	'Kirki',
// 	array(
// 		'type'     => 'text',
// 		'settings' => 'hero_title',
// 		'label'    => __( 'Banner Title', 'odin' ),
// 		'section'  => 'odin_hero_home',
// 	)
// );

// Kirki::add_field(
// 	'Kirki',
// 	array(
// 		'type'     => 'text',
// 		'settings' => 'hero_btn_txt',
// 		'label'    => __( 'Type for enable a action button', 'odin' ),
// 		'section'  => 'odin_hero_home',
// 	)
// );

// Kirki::add_field(
// 	'Kirki',
// 	array(
// 		'type'     => 'url',
// 		'settings' => 'hero_btn_url',
// 		'label'    => __( 'Type a url link to action button', 'odin' ),
// 		'section'  => 'odin_hero_home',
// 	)
// );

// Kirki::add_field(
// 	'Kirki',
// 	array(
// 		'type'     => 'rádio-buttonset',
// 		'settings' => 'hero_align',
// 		'label'    => __( 'Set alignament of the banner content', 'odin' ),
// 		'section'  => 'odin_hero_home',
// 		'choices'  => array(
// 			'left'   => esc_attr__( 'Left', 'odin' ),
// 			'center' => esc_attr__( 'Center', 'odin' ),
// 			'right'  => esc_attr__( 'Right', 'odin' ),
// 		),
// 	)
// );

// Kirki::add_field(
// 	'Kirki',
// 	array(
// 		'type'     => 'rádio-buttonset',
// 		'settings' => 'hero_pos',
// 		'label'    => __( 'Set Position of the banner content', 'odin' ),
// 		'section'  => 'odin_hero_home',
// 		'choices'  => array(
// 			'left'   => esc_attr__( 'Left', 'odin' ),
// 			'center' => esc_attr__( 'Center', 'odin' ),
// 			'right'  => esc_attr__( 'Right', 'odin' ),
// 		),
// 	)
// );

// // Footer Layout
// Kirki::add_field( 'Kirki', array(
// 'type'        => 'radio-image',
// 'settings'    => 'foot_layout',
// 'label'       => esc_html__( 'Layout options', 'odin' ),
// 'section'     => 'odin_foot_layout',
// 'default'     => '1',
// 'priority'    => 10,
// 'choices'     => array(
// '1'   => get_template_directory_uri() . '/assets/img/icons/foot-layout.png',
// '0' => get_template_directory_uri() . '/assets/img/icons/foot-layout2.png',
// ),
// ) );
// // Switch Menu on footer
// Kirki::add_field( 'Kirki', array(
// 'type'        => 'switch',
// 'settings'    => 'foot_menunav',
// 'label'       => __( 'Show Mini menu in Footer?', 'odin' ),
// 'section'     => 'odin_foot_layout',
// 'default'     => '1',
// 'priority'    => 10,
// 'choices'     => array(
// 'yes'  => __( 'Yes', 'odin' ),
// 'no' => __( 'No', 'odin' ),
// ),
// ) );
// Switch Mini reg on footer
// Kirki::add_field(
// 	'Kirki',
// 	array(
// 		'type'     => 'switch',
// 		'settings' => 'foot_minireg',
// 		'label'    => __( 'Show Mini Register in Footer?', 'odin' ),
// 		'section'  => 'odin_foot_misc',
// 		'default'  => '1',
// 		'priority' => 10,
// 		'choices'  => array(
// 			'yes' => __( 'Yes', 'odin' ),
// 			'no'  => __( 'No', 'odin' ),
// 		),
// 	)
// );

function odin_customize_init( $wp_customize ) {

	// Desible Core Customizer Panels
	$wp_customize->remove_section( 'static_front_page' );
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'colors' );

}

	add_action( 'customize_register', 'odin_customize_init' );
