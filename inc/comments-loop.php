<?php
if ( ! function_exists( 'odin_comment_loop' ) ) {

	/**
	 * Custom comments loop.
	 *
	 * @since 2.2.0
	 *
	 * @param  object $comment Comment object.
	 * @param  array  $args    Comment arguments.
	 * @param  int    $depth   Comment depth.
	 */
	function odin_comments_loop( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;

		switch ( $comment->comment_type ) {
			case 'pingback' :
			case 'trackback' :
?>
				<li class="media post pingback">
					<p><?php _e( 'Pingback:', 'odin' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'odin' ), '<span class="edit-link">', '</span>' ); ?></p>
<?php
			break;
			default :
?>
				<li <?php comment_class( '' ); ?> id="li-comment-<?php comment_ID(); ?>">
					<article id="div-comment-<?php comment_ID(); ?>" class="media comment-body comment-author vcard mb-3">
						
							<?php echo str_replace( "class='avatar", "class='media-object rounded-circle avatar mr-2", get_avatar( $comment, 64 ) ); ?>
						
						<div class="media-body p-3 card">
							<header class="comment-meta">
								<h5 class="media-heading py-2">
									<?php echo sprintf( '<strong><span class="d-block fn">%1$s</span></strong>
														 <small>%2$s <a href="%3$s"><time datetime="%4$s">%5$s %6$s </time></a>
														 <span class="says"> %7$s</span></small>',
														 get_comment_author_link(), __( 'in', 'odin' ),
														 esc_url( get_comment_link( $comment->comment_ID ) ),
														 get_comment_time( 'c' ),
														 get_comment_date(), __( 'at', 'odin' ),
														 get_comment_time(), __( 'said:', 'odin' ) ); ?>
								</h5>

								<?php if ( $comment->comment_approved == '0' ) : ?>
								<p class="comment-awaiting-moderation alert alert-info"><?php _e( 'Your comment is awaiting moderation.', 'odin' ); ?></p>
								<?php endif; ?>
							</header><!-- .comment-meta -->

							<div class="comment-content">
								<?php comment_text(); ?>
							</div><!-- .comment-content -->

							<div class="comment-metadata btn-group" role="group">
								<?php edit_comment_link( __( 'Edit', 'odin' ), '', '' ); ?>
								<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Respond', 'odin' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
							</div><!-- .comment-metadata -->
						</div>
					</article><!-- .comment-body -->
<?php
			break;
		}
	}
}
