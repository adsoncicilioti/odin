<?php
/**
 * Register nav menus.
 *
 * @since 2.2.0
 */
register_nav_menus(
	array(
		'main-menu' => __( 'Main Menu', 'odin' )
	)
);

// Permite html no campo de descrção
remove_filter('nav_menu_description', 'strip_tags');

add_filter( 'wp_setup_nav_menu_item', 'setup_menu_desc' );
function setup_menu_desc($menu_item) {
	$menu_item->description = apply_filters('nav_menu_description',  $menu_item->post_content );
	return $menu_item;
}
