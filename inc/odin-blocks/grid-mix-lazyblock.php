<?php

/**
 * FrontEnd Render Callback
 *
 * @param array $attrs - block attributes.
 */
function gm_render_frontend_cb( $attrs ) {

	$uid = uniqid( 'grid-mix-' );

	$gm_cols         = $attrs['gm_columns'];
	$gm_no_gut       = isset( $attrs['gm_no_gutters'] ) ? $attrs['gm_no_gutters'] : '';
	$gm_enab_excerpt = isset( $attrs['gm_enab_excerpt'] ) ? $attrs['gm_enab_excerpt'] : '';
	$gm_bg_color     = ! empty( $attrs['gm_bg_color'] ) ? 'background-color:' . $attrs['gm_bg_color'] . ';' : '';
	$gm_bg_items     = ! empty( $attrs['gm_bg_items'] ) ? 'background-color:' . $attrs['gm_bg_items'] . ';' : '';
	$gm_color_items  = ! empty( $attrs['gm_color_items'] ) ? 'color:' . $attrs['gm_color_items'] . ';' : '';
	$pad             = $gm_no_gut ? 'p-0' : 'p-3'
	?>

		<div id="<?php echo esc_attr( $uid ); ?>" class="<?php echo esc_attr( $pad ); ?>">

			<?php
			$style  = '';
			$style .= '<style>';
			$style .= $gm_bg_color ? '	#' . $uid . ' { ' . $gm_bg_color . ' }' : '';
			$style .= ( ! empty( $gm_bg_items ) || ! empty( $gm_color_items ) ) ? '	#' . $uid . ' .gm-inner { ' : '';
			$style .= ! empty( $gm_bg_items ) ? $gm_bg_items : '';
			$style .= ! empty( $gm_color_items ) ? $gm_color_items : '';
			$style .= ( ! empty( $gm_bg_items ) || ! empty( $gm_color_items ) ) ? '}' : '';
			$style .= '</style>';
			echo $style;
			?>

			<div class="grid-mix m-0 row <?php echo esc_attr( $gm_no_gut ); ?>">

		<?php
		$gm_items = is_array( $attrs['gm_setup'] ) ? $attrs['gm_setup'] : '';

		// LOOP - Grid Items
		if ( $gm_items ) {
			foreach ( $gm_items as $key => $grid_item ) {
				global $post;

				// Set URL
				$grid_url = isset( $grid_item['gm_item'] ) ? $grid_item['gm_item'] : '#';

				// Get ID from URl
				$gm_item_id = substr( $grid_url, 0, strlen( site_url() ) ) === site_url() ? url_to_postid( $grid_item['gm_item'] ) : '';

				// Set the Title
				$grid_title = $gm_item_id ? get_the_title( $gm_item_id ) : 'Título não definido';
				$grid_title = isset( $grid_item['gm_title'] ) && ! empty( $grid_item['gm_title'] ) ? $grid_item['gm_title'] : $grid_title;

				// Set the Description
				$grid_desc = ( ! empty( $gm_item_id ) && $gm_enab_excerpt ) ? get_the_excerpt_byid( $gm_item_id, 10 ) : '';
				$grid_desc = isset( $grid_item['gm_desc'] ) && ! empty( $grid_item['gm_desc'] ) ? $grid_item['gm_desc'] : $grid_desc;

				// Set the Image Thumbnail
				$setthumb  = ( ! empty( $grid_item['gm_img'] ) ) ? $grid_item['gm_img']['id'] : '';
				$autothumb = $gm_item_id ? get_post_thumbnail_id( $gm_item_id ) : '';
				$thumb     = $setthumb ? $setthumb : $autothumb;
				$grid_img  = $thumb ? 'background-image: url(' . odin_get_image_url( $thumb, 500, 380, true, true ) . ');' : '';

				/**
				 * Setup the HTMl of item
				 */
				$html_item  = '';
				$html_item .= '<div class="d-flex col-12 col-sm-6 col-md-' . $gm_cols . ' gm-item gm-item-' . $key . ' ' . $pad . '">';
				$html_item .= '<a class="d-flex gm-inner rel txt-c over-hide p-4 ease" href="' . $grid_url . '">';
				$html_item .= '<h3 class="h5 z1 w-100  ">' . $grid_title . '</h3>';
				$html_item .= $grid_desc ? '<span class="z1 gm-desc">' . $grid_desc . '</span>' : '';
				$html_item .= '</a>';
				$html_item .= ! empty( $thumb ) ? '<style>	#' . $uid . ' .gm-item-' . $key . ' .gm-inner:before {' . $grid_img . '	}</style>' : '';
				$html_item .= '</div>';

				echo $html_item;

			}
		}
		?>
			</div>
		</div>

		<?php
}

	lazyblocks()->add_block(
		array(
			'id'             => 3647,
			'title'          => 'Grid Mix',
			'icon'           => 'dashicons dashicons-forms',
			'keywords'       => array(
				0 => 'grid',
				1 => 'mix',
			),
			'slug'           => 'lazyblock/grid-mix',
			'description'    => 'Grid de itens existentes no site: Posts e páginas com liberdade de organização.',
			'category'       => 'common',
			'category_label' => 'common',
			'supports'       => array(
				'customClassName' => true,
				'anchor'          => true,
				'align'           => array(
					0 => 'wide',
					1 => 'full',
				),
				'html'            => false,
				'multiple'        => false,
				'inserter'        => true,
			),
			'controls'       => array(
				'control_47d85543f1' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Inserir Item',
					'name'              => 'gm_setup',
					'type'              => 'repeater',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'content',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_07dba148b7' => array(
					'sort'              => '',
					'child_of'          => 'control_47d85543f1',
					'label'             => 'Encontre um item',
					'name'              => 'gm_item',
					'type'              => 'url',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placeholder'       => '',
					'help'              => 'Digite algo e sugestões de conteúdo existente vão aparecer, ou cole qualquer link de sua escolha.',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_975b004545' => array(
					'sort'              => '',
					'child_of'          => 'control_47d85543f1',
					'label'             => 'Título Específico',
					'name'              => 'gm_title',
					'type'              => 'text',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placeholder'       => '',
					'help'              => 'Se o item for um link de uma página ou post interno o titulo correspondente será carregado automaticamente.',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_6b1b7f449f' => array(
					'sort'              => '',
					'child_of'          => 'control_47d85543f1',
					'label'             => 'Descrição',
					'name'              => 'gm_desc',
					'type'              => 'textarea',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placeholder'       => '',
					'help'              => 'Texto complementar ao titulo. Para Mostrar Resumo automático ative na barra lateral.',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_d78afe44b9' => array(
					'sort'              => '',
					'child_of'          => 'control_47d85543f1',
					'label'             => 'Imagem',
					'name'              => 'gm_img',
					'type'              => 'image',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placeholder'       => '',
					'help'              => 'Se o item for um post/página com imagem destacada definida ela será mostrada automaticamente',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_ce1a7c4447' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Número de colunas',
					'name'              => 'gm_columns',
					'type'              => 'select',
					'choices'           => array(
						array(
							'value' => '6',
							'label' => '2 colunas',
						),
						array(
							'value' => '4',
							'label' => '3 colunas',
						),
						array(
							'value' => '3',
							'label' => '4 colunas',
						),
					),
					'min'               => '1',
					'max'               => '4',
					'step'              => '1',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '4',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_e0499f4817' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Sem espaçamento',
					'name'              => 'gm_no_gutters',
					'type'              => 'toggle',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_b9fbb84591' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Mostrar resumo como descrição',
					'name'              => 'gm_enab_excerpt',
					'type'              => 'toggle',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placeholder'       => '',
					'help'              => 'Se o item for de um link de post/página existentes, ele pegará um pequeno trecho inicial de seu conteúdo.',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_f3eb554ffb' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Cor de fundo',
					'name'              => 'gm_bg_color',
					'type'              => 'color',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_cd2aa74997' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Cor do Fundo dos Items',
					'name'              => 'gm_bg_items',
					'type'              => 'color',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_5aab424bed' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Cor dos Textos dos items',
					'name'              => 'gm_color_items',
					'type'              => 'color',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
			),
			'code'           => array(
				'editor_html'       => '
				<div class="container{{#if gm_no_gutters}} p-0{{else}} py-3{{/if}}" style="background-color:{{gm_bg_color}};">
					<div class="grid-mix m-0 row{{#if gm_no_gutters}} no-gutters{{/if}}">
					{{#each gm_setup}}
					<div class="gm-item gm-item-{{@index}} d-flex col-12 col-sm-6 col-md-{{../gm_columns}}{{#if ../gm_no_gutters}} py-0{{else}} py-3{{/if}}">
						<div class="gm-inner d-flex rel over-hide flex-fill p-4 flex-column justify-content-center txt-c text-white bg-navy" {{#if ../gm_bg_items}}style="background-color:{{../gm_bg_items}} !important;"{{/if}} >

							<style>
							{{#if gm_img}}
							.grid-mix .gm-item-{{@index}} > div::before{
								background-image: url(\'{{gm_img.url}}\');
							}
							{{/if}}
							{{#if ../gm_color_items}}
							.grid-mix .gm-item-{{@index}} .gm-inner {
								color: {{../gm_color_items}} !important;
							}
							{{/if}}
							</style>

						{{#if gm_title}}
						<h3 class="z1 h5 w-100">{{gm_title}}</h3>
						{{else}}
						<h3 class="z1 h5 w-100">Defina um título ou deixe automático</h3>
						{{/if}}
						{{#if gm_desc}}
						<span class="z1 w-100">{{gm_desc}}</span>
						{{else}}
						<span class="lead z1 w-100">Sua descrição aqui</span>
						{{/if}}
						</div>
					</div>
					{{/each}}
					</div>
				</div>',
				'editor_css'        => '',
				'frontend_callback' => 'gm_render_frontend_cb',
				'frontend_html'     => '',
				'frontend_css'      => '',
			),
			'condition'      => array(
				0 => 'post',
				1 => 'page',
			),
		)
	);
