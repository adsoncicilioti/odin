<?php

/**
 * FrontEnd Render Callback
 *
 * @param array $attrs - block attributes.
 */
function cta_render_frontend_cb( $attrs ) {

	$uid = uniqid( 'cta_' );
	// Set title
	$cta_title = $attrs['cta_title'] ? $attrs['cta_title'] : '';

	// Set description
	$cta_text = $attrs['cta_text'] ? $attrs['cta_text'] : '';

	// Set btn
	$cta_btn_txt   = $attrs['cta_btn_txt'] ? $attrs['cta_btn_txt'] : '';
	$cta_btn_color = ' ' . $attrs['cta_btn_color'];
	$cta_btn_url   = $attrs['cta_btn_url'] ? $attrs['cta_btn_url'] : '';
	$cta_open      = $attrs['cta_open'] ? ' target="_blank"' : '';

	// Set btn icon
	$cta_icon_btn   = $attrs['cta_icon_btn'];
	$cta_icon_color = 'none' !== $attrs['cta_icon_color'] ? ' ' . $attrs['cta_icon_color'] : '';

	// Set texts align
	$cta_txt_align = $attrs['cta_txt_align'] ? ' text-' . $attrs['cta_txt_align'] : '';

	// Set img
	$cta_img_size = $attrs['cta_img_size'] ? $attrs['cta_img_size'] : '';
	$cta_img      = isset( $attrs['cta_img']['id'] ) ? wp_get_attachment_image( $attrs['cta_img']['id'], $cta_img_size ) : '';

	// Set img alig
	$cta_img_align = $attrs['cta_img_align'];

	// Set bg color
	$cta_bg_color = ! empty( $attrs['cta_bg_color'] ) ? 'background-color:' . $attrs['cta_bg_color'] . ';' : '';

	// Set texts color
	$cta_txt_color = $attrs['cta_txt_color'] ? 'color:' . $attrs['cta_txt_color'] : ';';

	// Set Custom CSS
	$cta_ext_css = $attrs['cta_ext_css'] ? $attrs['cta_ext_css'] : '';

	?>

		<section id="<?php echo esc_attr( $uid ); ?>" class="wp-block-cta call-to-action d-flex align-items-center<?php echo esc_attr( ' ' . $cta_img_align ); ?> p-3" <?php echo esc_attr( $cta_bg_color ); ?>>

		<?php

			// Setup the HTMl
			$html  = '';
			$html .= '
			<style>
			#' . $uid . ' {
				' . $cta_bg_color . '
			}
			#' . $uid . ' .cta-title,
			#' . $uid . ' .cta-txt {
				' . $cta_txt_color . '
			}
			' . $cta_ext_css . '
			</style>
			';

			$html .= $cta_img ? '<div class="call-img p-3 ' . ( 'flex-column' === $cta_img_align ? ' pt-5' : '' ) . '">' . $cta_img . '</div>' : '';
			// $html .= $cta_img ? '</div>' : '';

			$html .= '<div class="call-wrap p-4 flex-fill m-0' . $cta_txt_align . '">'; // start call-wrap

			$html .= ! empty( $cta_title ) ? '<h2 class="cta-title h3">' . $cta_title . '</h2>' : '';
			$html .= ! empty( $cta_text ) ? '<p class="cta-txt lead">' . $cta_text . '</p>' : '';

			$html .= ! empty( $cta_btn_txt ) ? '<a href="' . $cta_btn_url . '" class="btn' . $cta_btn_color . '"' . $cta_open . '>' . $cta_btn_txt : '';
			$html .= ! empty( $cta_btn_txt ) && 'none' !== $cta_icon_btn ? '<svg class="icon ml-3 mb-0 h4' . $cta_icon_color . '"><use xlink:href="#' . $cta_icon_btn . '" /></svg>' : '';
			$html .= $cta_btn_txt ? '</a>' : '';

			$html .= '</div>'; // end call-wrap

			echo wp_filter_content_tags( $html );
		?>
		</section>

		<?php
}

	lazyblocks()->add_block(
		array(
			'id'             => 4143,
			'title'          => 'Chamar para ação',
			'icon'           => 'dashicons dashicons-megaphone',
			'keywords'       => array(
				0 => 'cta',
				1 => 'call to action',
			),
			'slug'           => 'lazyblock/call-to-action',
			'description'    => 'Bloco para criar uma seção de Chamada para ação.',
			'category'       => 'layout',
			'category_label' => 'layout',
			'supports'       => array(
				'customClassName' => true,
				'anchor'          => true,
				'align'           => array(
					0 => 'wide',
					1 => 'full',
				),
				'html'            => false,
				'multiple'        => true,
				'inserter'        => true,
			),
			'controls'       => array(
				'control_adaa374453' => array(
					'sort'              => '1',
					'child_of'          => '',
					'label'             => 'Título',
					'name'              => 'cta_title',
					'type'              => 'text',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'content',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_88bad84ad2' => array(
					'sort'              => '2',
					'child_of'          => '',
					'label'             => 'Texto de Descrição',
					'name'              => 'cta_text',
					'type'              => 'textarea',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'content',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_6648974854' => array(
					'sort'              => '3',
					'child_of'          => '',
					'label'             => 'Texto do botão',
					'name'              => 'cta_btn_txt',
					'type'              => 'text',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'content',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_4dba3a4581' => array(
					'sort'              => '4',
					'child_of'          => '',
					'label'             => 'Abrir link em nova janela',
					'name'              => 'cta_open',
					'type'              => 'toggle',
					'allow_null'        => 'false',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placeholder'       => '',
					'help'              => '',
					'placement'         => 'content',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_9c19b64496' => array(
					'sort'              => '5',
					'child_of'          => '',
					'label'             => 'Link do Botão',
					'name'              => 'cta_btn_url',
					'type'              => 'url',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_4919d84a50' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Cor do Botão',
					'name'              => 'cta_btn_color',
					'type'              => 'select',
					'choices'           => array(
						array(
							'value' => 'btn-primary',
							'label' => 'Cor Primaria do Site',
						),
						array(
							'value' => 'btn-light',
							'label' => 'Claro (quase branco)',
						),
						array(
							'value' => 'btn-secondary',
							'label' => 'Grafite',
						),
						array(
							'value' => 'btn-dark',
							'label' => '(quase preto)',
						),
						array(
							'value' => 'btn-warning',
							'label' => 'Amarelo',
						),
						array(
							'value' => 'btn-danger',
							'label' => 'Vermelho',
						),
						array(
							'value' => 'btn-success',
							'label' => 'Verde',
						),
						array(
							'value' => 'btn-info',
							'label' => 'Turquesa',
						),
					),
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => 'btn-primary',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_4dba0d4921' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Ícone do Botão',
					'name'              => 'cta_icon_btn',
					'type'              => 'select',
					'choices'           => array(
						array(
							'value' => 'none',
							'label' => 'Nenhum',
						),
						array(
							'value' => 'message',
							'label' => 'Balão de Chat',
						),
						array(
							'value' => 'check',
							'label' => 'Checagem',
						),
						array(
							'value' => 'download',
							'label' => 'Download',
						),
						array(
							'value' => 'chevron-left',
							'label' => 'Esquerda',
						),
						array(
							'value' => 'chevron-right',
							'label' => 'Direita',
						),
						array(
							'value' => 'paper-plane',
							'label' => 'Enviar',
						),
						array(
							'value' => 'envelope',
							'label' => 'E-mail',
						),
						array(
							'value' => 'info',
							'label' => 'Informação',
						),
						array(
							'value' => 'triangle-danger',
							'label' => 'Perigo',
						),
					),
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => 'none',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_c2fb864c1e' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Cor do Ícone',
					'name'              => 'cta_icon_color',
					'type'              => 'select',
					'choices'           => array(
						array(
							'value' => 'none',
							'label' => 'Igual ao texto do botão',
						),
						array(
							'value' => 'text-black-50',
							'label' => 'Marca Escura',
						),
						array(
							'value' => 'text-white-50',
							'label' => 'Marca Clara',
						),
						array(
							'value' => 'text-primary',
							'label' => 'cor Primária do site',
						),
						array(
							'value' => 'text-white',
							'label' => 'Branco',
						),
						array(
							'value' => 'text-secondary',
							'label' => 'Grafite',
						),
						array(
							'value' => 'text-dark',
							'label' => 'Escuro (quase preto)',
						),
						array(
							'value' => 'text-danger',
							'label' => 'Vermelho',
						),
						array(
							'value' => 'text-warning',
							'label' => 'Amarelo',
						),
						array(
							'value' => 'text-success',
							'label' => 'Verde',
						),
						array(
							'value' => 'text-info',
							'label' => 'Turquesa',
						),
					),
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_6d2a9044ef' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Alinhamento dos textos',
					'name'              => 'cta_txt_align',
					'type'              => 'select',
					'choices'           => array(
						array(
							'value' => 'center',
							'label' => 'Centro',
						),
						array(
							'value' => 'left',
							'label' => 'Esquerda',
						),
						array(
							'value' => 'right',
							'label' => 'Direita',
						),
					),
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => 'center',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_0fb95b4d70' => array(
					'sort'              => '6',
					'child_of'          => '',
					'label'             => 'Imagem de Destaque',
					'name'              => 'cta_img',
					'type'              => 'image',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_c669f94628' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Tamanho da imagem',
					'name'              => 'cta_img_size',
					'type'              => 'select',
					'choices'           => array(
						array(
							'value' => 'thumbnail',
							'label' => 'Miniatura',
						),
						array(
							'value' => 'medium',
							'label' => 'Média',
						),
						array(
							'value' => 'medium_large',
							'label' => 'Intermediária',
						),
						array(
							'value' => 'large',
							'label' => 'Grande',
						),
						array(
							'value' => 'full',
							'label' => 'Original',
						),
					),
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => 'thumbnail',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_30b9624bbe' => array(
					'sort'              => '7',
					'child_of'          => '',
					'label'             => 'Alinhamento da Imagem',
					'name'              => 'cta_img_align',
					'type'              => 'select',
					'choices'           => array(
						array(
							'value' => 'flex-column',
							'label' => 'Centro',
						),
						array(
							'value' => 'flex-row',
							'label' => 'Esquerda',
						),
						array(
							'value' => 'flex-row-reverse',
							'label' => 'Direita',
						),
					),
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => 'flex-row',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_9aebc14e41' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'Cor do texto',
					'name'              => 'cta_txt_color',
					'type'              => 'color',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_9f4abe4855' => array(
					'sort'              => '8',
					'child_of'          => '',
					'label'             => 'Cor de Fundo',
					'name'              => 'cta_bg_color',
					'type'              => 'color',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
				'control_5bb8374ac7' => array(
					'sort'              => '',
					'child_of'          => '',
					'label'             => 'CSS Personalizado (conhecimentos de desenvolvedor necessários)',
					'name'              => 'cta_ext_css',
					'type'              => 'code_editor',
					'min'               => '',
					'max'               => '',
					'step'              => '',
					'date_time_picker'  => 'date_time',
					'multiline'         => 'false',
					'default'           => '',
					'checked'           => 'false',
					'placement'         => 'inspector',
					'save_in_meta'      => 'false',
					'save_in_meta_name' => '',
				),
			),
			'code'           => array(
				'editor_html'       => '',
				'editor_css'        => '',
				'frontend_html'     => '',
				'frontend_callback' => 'cta_render_frontend_cb',
				'frontend_css'      => '',
			),
			'condition'      => array(
				0 => 'post',
				1 => 'page',
			),
		)
	);
