<?php
if ( function_exists( 'lazyblocks' ) ) :
	require_once get_template_directory() . '/inc/odin-blocks/grid-mix-lazyblock.php';
	require_once get_template_directory() . '/inc/odin-blocks/call-to-action-lazyblock.php';
endif;
