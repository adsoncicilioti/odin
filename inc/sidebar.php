<?php
/**
 * Register widget areas.
 *
 * @since 2.2.0
 */
function odin_widgets_init() {
	register_sidebar(
		array(
			'name' => __( 'Main Sidebar', 'odin' ),
			'id' => 'main-sidebar',
			'description' => __( 'Site Main Sidebar', 'odin' ),
			'before_widget' => '<aside id="%1$s" class="py-3 widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="mb-3 widgettitle widget-title">',
			'after_title' => '</h3>',
		)
	);

	// register_sidebar(
	// 	array(
	// 		'name' => __( 'Home Área', 'odin' ),
	// 		'id' => 'home_area',
	// 		'description' => __( 'Widget for Home page', 'odin' ),
	// 		'before_widget' => '<section id="%1$s" class="widget %2$s">',
	// 		'after_widget' => '</section>',
	// 		'before_title' => '<h2 class="widgettitle widget-title">',
	// 		'after_title' => '</h2>',
	// 	)
	// );

}

add_action( 'widgets_init', 'odin_widgets_init' );
