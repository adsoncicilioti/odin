<?php

function odin_contact_form() {

    $form = new Odin_Contact_Form(
        'contact_form',
        'contato@frutasbrasilsul.com.br',
        array( '' ),
        array( '' )
    );

    $form->set_fields(
        array(
            array(
                'fields' => array(
                    array(
                        'id'          => 'sender_name', // Required
                        'label'       => __( 'Nome', 'odin' ), // Required
                        'type'        => 'text', // Required
                        'required'    => true, // Optional (bool)
                        'attributes'  => array( // Optional (html input elements)
                            'placeholder' => __( 'Digite o seu nome' )
                        )
                    ),
                    array(
                        'id'          => 'sender_email', // Required
                        'label'       => __( 'E-mail', 'odin' ), // Required
                        'type'        => 'email', // Required
                        'required'    => true, // Optional (bool)
                        'attributes'  => array( // Optional (html input elements)
                            'placeholder' => __( 'Digite o seu e-mail!' )
                        ),
                    ),
                    array(
                        'id'          => 'sender_subject', // Required
                        'label'       => __( 'Assunto', 'odin' ), // Required
                        'type'        => 'text', // Required
                        'required'    => true, // Optional (bool)
                        'attributes'  => array( // Optional (html input elements)
                            'placeholder' => __( 'Qual o assunto da mensagem?' )
                        )
                    ),
                    array(
                        'id'          => 'sender_message', // Required
                        'label'       => __( 'Mensagem', 'odin' ), // Required
                        'type'        => 'textarea', // Required
                        'required'    => true, // Optional (bool)
                        'attributes'  => array( // Optional (html input elements)
                            'placeholder' => __( 'Digite a sua mensagem' )
                        ),
                    )/*,
                    array(
                        'id'          => 'sender_file', // Required
                        'label'       => __( 'Arquivo', 'odin' ), // Required
                        'type'        => 'file', // Required
                        'required'    => false, // Optional (bool)
                    )*/
                )
            )
        )
    );

    $form->set_subject( __( '[sender_subject] - E-mail de [sender_name] <[sender_email]>', 'odin' ) );

    $form->set_content_type( 'html' );

    $form->set_reply_to( 'sender_email' );

    return $form;
}

add_action( 'init', array( odin_contact_form(), 'init' ), 1 );

// Add Shortcode
function contact_form_shortcode() {
    $contact_form = odin_contact_form()->render();
    return $contact_form;
}
add_shortcode( 'contact_form', 'contact_form_shortcode' );
