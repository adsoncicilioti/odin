<?php

function odin_portfolio_cpt() {
    $portfolio = new Odin_Post_Type(
        'Cliente', // Nome (Singular) do Post Type.
        'portfolio' // Slug do Post Type.
    );

    $portfolio->set_labels(
        array(
            'menu_name' => __( 'Portfolio', 'odin' )
        )
    );

    $portfolio->set_arguments(
        array(
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' ),
            'menu_icon' => 'dashicons-index-card'
        )
    );
}

add_action( 'init', 'odin_portfolio_cpt', 1 );


//// RECENT PORTIFOLIO ITENS
function odin_recent_portifolio() {
  global $post;

  $html = '';

  $my_query = new WP_Query( array(
       'post_type' => 'portfolio',
       'showposts' => 6
  ));
  $html .= '<div class="portfolio clearfix">';


  if( $my_query->have_posts() ) : while( $my_query->have_posts() ) : $my_query->the_post();

    if ( has_post_thumbnail() ) {
        $thumb = '<a href="' . esc_url( get_permalink() ) . '" title="'.get_the_title().'" rel="bookmark" id="post-' .  get_the_ID() . '" class="col-xs-6 col-md-4 p-0 portfolio-item relative sr-t ease ' . implode(' ', get_post_class('', $post->ID)) . '">' . odin_thumbnail( 300, 180, get_the_title(), true, 'ease' ) . '</a>';
    }
    else {
        $thumb =  '<a href="' . esc_url( get_permalink() ) . '" title="'.get_the_title().'" rel="bookmark" id="post-' .  get_the_ID() . '" class="col-xs-6 col-md-4 p-0 portfolio-item relative ' . implode(' ', get_post_class('', $post->ID)) . '"><img class="ease" src="' . get_template_directory_uri() . '/assets/images/nothumb.jpg" alt="' . __( 'Sem imagem','odin' ) . '"/></a>';
    }

    $html .=  $thumb;

  endwhile;

  $html .= '</div>';

  wp_reset_postdata();
  endif;

  return $html;
 }
 add_shortcode( 'portfolio', 'odin_recent_portifolio' );
