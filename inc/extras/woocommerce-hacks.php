<?php

// Remove prices

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

// Remove prices everywhere - except cart and checkout

add_filter( 'woocommerce_variable_sale_price_html', 'odin_remove_prices', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'odin_remove_prices', 10, 2 );
add_filter( 'woocommerce_get_price_html', 'odin_remove_prices', 10, 2 );
add_filter( 'woocommerce_grouped_price_html','odin_remove_prices',10, 2);
add_filter( 'woocommerce_variation_price_html','odin_remove_prices',10, 2);
add_filter( 'woocommerce_variation_sale_price_html','odin_remove_prices',10, 2);
add_filter( 'woocommerce_variation_free_price_html','odin_remove_prices',10, 2);
add_filter( 'woocommerce_variation_empty_price_html','odin_remove_prices',10, 2);

function odin_remove_prices( $price ) {
$price = '';
return $price;
}


// ENABLE EDIT ORDERS
add_filter( 'wc_order_is_editable', 'wc_make_processing_orders_editable', 10, 2 );
function wc_make_processing_orders_editable( $is_editable, $order ) {
    // if ( $order->get_status() == 'processing' ) {
        $is_editable = true;
    // }
    return $is_editable;
}

// Remove description after categories  title
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );


// CHANGE Continue Shopping Button
add_filter( 'wc_add_to_cart_message', 'odin_add_to_cart_message', 10 , 2 );
function odin_add_to_cart_message( $message, $product_id ) {
    global $woocommerce;
    	//$product_id = $_REQUEST[ 'product_id' ];
        $return_to  = get_permalink(woocommerce_get_page_id('shop'));
        $message    = sprintf( __('<strong>&quot;%s&quot;</strong> foi adicionado com sucesso ao seu orçamento. <a href="%s" class="button wc-forwards">%s</a>', 'odin'), get_the_title( $product_id ) , $return_to, __('Adicionar mais itens', 'odin') );
    return $message;
}

// Modify the default WooCommerce orderby dropdown
//
// Options: menu_order, popularity, rating, date, price, price-desc
// In this example I'm removing price & price-desc but you can remove any of the options
function odin_woocommerce_catalog_orderby( $orderby ) {
	unset($orderby["price"]);
	unset($orderby["price-desc"]);
	return $orderby;
}
add_filter( 'woocommerce_catalog_orderby', 'odin_woocommerce_catalog_orderby', 20 );

// remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

//Change text Button per types
function custom_woocommerce_product_add_to_cart_text( ) {
	global $post;
	$product = get_object_taxonomies( 'product' );
	$prod = $product[0];

	$types = get_the_terms( $post->ID, $product[0] );
	$type = $types ? array_shift( $types ) : '';
	$product_type =  $type->slug;
	if ( $product_type == 'simple' ) {
		$text = __( 'Orçar', 'odin' );

	} elseif ( $product_type == 'variable' ) {
		$text = __( 'Ver Opções', 'odin' );

	} else {
		$text = __( 'Ver mais', 'odin' );
	}
	return $text;
}
//Change Text of Add cart Button
//On Single and Product Archives
add_action('woocommerce_product_add_to_cart_text', 'custom_woocommerce_product_add_to_cart_text'); // 2.1 +

/*REPLACE ADD CART BUTTON*/
function remove_loop_button(){
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
}
add_action('init','remove_loop_button');

add_action('woocommerce_after_shop_loop_item','replace_add_to_cart');
function replace_add_to_cart() {
global $product;
$link = $product->get_permalink();
echo '<a href="' . esc_attr($link) . '" class="btn btn-primary mb-3">' . __('Orçar', 'odin') . '</a>';
}


/**
* Change Proceed To Checkout Text in WooCommerce
* Place this in your Functions.php file
**/
function woocommerce_button_proceed_to_checkout() {
	$checkout_url =   wc_get_checkout_url();
	echo '<a href="'.$checkout_url.'" class="checkout-button button alt wc-forward">'; echo _e( 'Finalizar Orçamento', 'odin' ); echo '</a>';
}

//Botão Finalizar Compra CHECKOUT PAGE - Texto // input > button
add_filter( 'woocommerce_order_button_html', 'odin_order_button');
function odin_order_button($odin_button) {

	$odin_button = '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order"  data-value="' . __('Pedir Orçamento', 'odin') . '">' . __('Pedir Orçamento', 'odin') . '</button>';
	return $odin_button;
}//Fim btn finilizar compra

/// PRODUCT SINGLE BTN TEXT
add_filter('woocommerce_product_single_add_to_cart_text', 'odin_product_single_add_to_cart_text');
function odin_product_single_add_to_cart_text() {
	return __( 'Orçar', 'odin' );
}


////////////  RELOCATE PRODUCT TITLE FOR THEME   /////////////////
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );

add_action( 'woocommerce_before_single_product', 'odin_product_title' );
// PARA AJUSTAR o Elemento Pai Procure no arquivo inc/woocommerce/functions.php
function odin_product_title() {
 echo '<section class="page-header rel py-3"><div class="container"><h1 class="m-0 h2  product-title page-title">';
 echo the_title();
 echo '</h1></div></section>';
 if ( function_exists( 'odin_breadcrumbs' ) ) {
    echo '<div class="bread-wrap"><div class="container">';
    odin_breadcrumbs();
    echo '</div></div>';
 }

}

add_filter('gettext', 'translate_text');
add_filter('ngettext', 'translate_text');

function translate_text($translated) {
$translated = str_ireplace('Payment Method:', 'Método:', $translated);
return $translated;
}

// Remove Sidebar on Single Product
add_action('template_redirect', 'remove_sidebar_shop');
function remove_sidebar_shop() {
if ( is_product() ) {
    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar');
    }
}

//Set/Unset Tabs
// add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
// function woo_remove_product_tabs( $tabs ) {
//     //unset( $tabs['description'] );      	// Remove the description tab
//     //unset( $tabs['reviews'] ); 			// Remove the reviews tab
//     unset( $tabs['additional_information'] );  	// Remove the additional information tab
//     return $tabs;
// }

//Reordring Tabs
add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

	$tabs['reviews']['priority'] = 10;			// Reviews first
	$tabs['description']['priority'] = 5;			// Description second
	// $tabs['additional_information']['priority'] = 15;	// Additional information third

	return $tabs;
}

////  RANAME WOOCOMMERCE ADMIN MENU
add_action( 'admin_menu', 'rename_woocoomerce', 999 );

function rename_woocoomerce() {
    global $menu;

    // Pinpoint menu item
    $woo = recursive_array_search( 'WooCommerce', $menu );

    // Validate
    if( !$woo )
        return;

    $menu[$woo][0] = 'Catálogo';
}

// http://www.php.net/manual/en/function.array-search.php#91365
function recursive_array_search( $needle, $haystack ) {
    foreach( $haystack as $key => $value )
    {
        $current_key = $key;
        if(
            $needle === $value
            OR (
                is_array( $value )
                && recursive_array_search( $needle, $value ) !== false
            )
        )
        {
            return $current_key;
        }
    }
    return false;
}

//////////// OVERRINDE MINI CART
if ( ! function_exists( 'woocommerce_mini_cart' ) ) {

 function woocommerce_mini_cart( $args = array() ) {

   $defaults = array( 'list_class' => '' );
   $args = wp_parse_args( $args, $defaults );
    wc_get_template( 'cart/mini-cart.php', $args );
 }
}

/*********************************************************************************************/
/** WooCommerce - Modify each individual input type $args defaults /**
/*********************************************************************************************/

add_filter('woocommerce_form_field_args','wc_form_field_args',10,3);

function wc_form_field_args( $args, $key, $value = null ) {

/*********************************************************************************************/
/** This is not meant to be here, but it serves as a reference
/** of what is possible to be changed. /**

$defaults = array(
    'type'              => 'text',
    'label'             => '',
    'description'       => '',
    'placeholder'       => '',
    'maxlength'         => false,
    'required'          => false,
    'id'                => $key,
    'class'             => array(),
    'label_class'       => array(),
    'input_class'       => array(),
    'return'            => false,
    'options'           => array(),
    'custom_attributes' => array(),
    'validate'          => array(),
    'default'           => '',
);
/*********************************************************************************************/

// Start field type switch case

switch ( $args['type'] ) {

    case "select" :  /* Targets all select input type elements, except the country and state select input types */
        $args['class'][] = 'form-group'; // Add a class to the field's html element wrapper - woocommerce input types (fields) are often wrapped within a <p></p> tag
        $args['input_class'] = array('form-control', 'input-lg'); // Add a class to the form input itself
        //$args['custom_attributes']['data-plugin'] = 'select2';
        $args['label_class'] = array('control-label');
        $args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  ); // Add custom data attributes to the form input itself
    break;

    case 'country' : /* By default WooCommerce will populate a select with the country names - $args defined for this specific input type targets only the country select element */
        $args['class'][] = 'form-group single-country';
        $args['label_class'] = array('control-label');
    break;

    case "state" : /* By default WooCommerce will populate a select with state names - $args defined for this specific input type targets only the country select element */
        $args['class'][] = 'form-group'; // Add class to the field's html element wrapper
        $args['input_class'] = array('form-control', 'input-lg'); // add class to the form input itself
        // $args['custom_attributes']['data-plugin'] = 'select2';
        $args['label_class'] = array('control-label');
        $args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  );
    break;


    case "password" :
    case "text" :
    case "email" :
    case "tel" :
    case "number" :
        $args['class'][] = 'form-group';
        //$args['input_class'][] = 'form-control input-lg'; // will return an array of classes, the same as bellow
        $args['input_class'] = array('form-control', 'input-lg');
        $args['label_class'] = array('control-label');
    break;

    case 'textarea' :
        $args['input_class'] = array('form-control', 'input-lg');
        $args['label_class'] = array('control-label');
    break;

    case 'checkbox' :
    break;

    case 'radio' :
    break;

    default :
        $args['class'][] = 'form-group';
        $args['input_class'] = array('form-control', 'input-lg');
        $args['label_class'] = array('control-label');
    break;
    }

    return $args;
}

// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
	// unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	// unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}

////////////////   custom loop
function odin_custom_loop( $atts, $html = null ) {
    global $post;

    extract( shortcode_atts( array(
        'type'          => 'post',
        'bgitens'       => 'bg-faded',
        'color'         => 'text-gray-dark',
        'columns'       => '3'
    ), $atts ) );

    $html = '';

    $html .= '<div class="loop row no-gutters mb-5">';

    $loop = new WP_Query( array(
       'post_type' => 'product',
       'posts_per_page' => 12,
       'order' => 'desc',
	   'orderby' => 'date'
			//  'offset' => 1
    ));

    // LOOP
    if( $loop->have_posts() ) : while( $loop->have_posts() ) : $loop->the_post();

    $thumb = ( has_post_thumbnail() ) ? '<div class="img-wrap rel mx-auto">' . odin_thumbnail( 380, 380, get_the_title(), false, 'img-fluid w-100' ) . '</div>' : '<img class="img-fluid" src="' . get_bloginfo( 'template_directory' ) . '/assets/img/nothumb.png" alt="' . __( 'No image','odin' ) . '"/>' ;

    $html .= '<article id="post-' .  get_the_ID() . '" class="col-sm-6 col-md-4 sr-fade d-flex' . implode(' ', get_post_class( ' ', $post->ID))  . '">';
	$html .= '<div class="wrap m-1 p-3">';
    $html .= '<a class="img-link mb-4" href="' . esc_url( get_permalink() ) . '" rel="bookmark">';
	$html .=  $thumb;
	$html .= '</a>';
	$html .= '<header>';
	$html .= '<h3 class="h5 txt-bold"><a class="text-gray-dark" href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . get_the_title() . '</a></h3>';
	$html .= '</header>';
	$html .= '</div></article>';

	endwhile;
	wp_reset_postdata();
    $html .= '</div>';
    endif;

    return $html;
}
add_shortcode( 'loop', 'odin_custom_loop' );

// MINIMUM QTY
// Simple products
add_filter( 'woocommerce_quantity_input_args', 'odin_woocommerce_quantity_input_args', 10, 2 );
function odin_woocommerce_quantity_input_args( $args, $product ) {
    // $args['max_value'] 		= 80; 	// Maximum value
    $args['min_value'] 		= 30;   	// Minimum value
    $args['step'] 		= 10;    // Quantity steps
    return $args;
}

// Variations
add_filter( 'woocommerce_available_variation', 'odin_woocommerce_available_variation' );
function odin_woocommerce_available_variation( $args ) {
	//$args['max_qty'] = 80; 		// Maximum value (variations)
    $args['min_qty'] = 30;   	// Minimum value (variations)
    return $args;
}

/**
 * Validate product quantity on cart update.
 */
function cs_update_validate_quantity( $valid, $cart_item_key, $values, $quantity ) {
    global $woocommerce;
    $min_quantity = 30;
    $valid = true;

    // Test quantity.
    if ( $min_quantity > $quantity ) {

        // Sets error message.
        $woocommerce->cart->set_quantity( $cart_item_key, $min_quantity );
        $valid = false;
    }
    return $valid;
}
add_filter( 'woocommerce_update_cart_validation', 'cs_update_validate_quantity', 1, 4 );

// Code for set minimum quantity
add_action( 'woocommerce_checkout_process', 'cart_contents_count' );
add_action( 'woocommerce_before_cart' , 'cart_contents_count' );
function cart_contents_count() {
	// Set this variable to specify a minimum order Quantity
	$minimum = 30;
	$qty = WC()->cart->cart_contents_count;
	if ( WC()->cart->cart_contents_count < $minimum ) {
		if( is_cart() ) { wc_print_notice(  sprintf( __('You must have an order with a minimum of %s quantity to place your order, your current order total quantity is %s.','odin') , $minimum, $qty ),'error');
		} else {
		wc_add_notice( sprintf( __('You must have an order with a minimum of %s quantity to place your order, your current order total quantity is %s.','odin'), $minimum, $qty ),'error');
		}
	}
}
/* End of code for setting minimum item quantity */
