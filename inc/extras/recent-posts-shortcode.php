<?php

//// RECENT POST SHORTCODE
function odin_recent_post( $atts, $html = '' ) {
	// Attributes
	$atts = shortcode_atts(
		array(
			'text'   => '',
			'type'   => 'post', // Pages or Other Post type
			'number' => '3',
			'style'  => 'grid', // or list
			'cols'   => '3',
		),
		$atts
	);

	$text = $atts['text'];
	$text = ( $text ) ? $text : esc_html( 'Recent Posts', 'odin' );

	$type = $atts['type'];
	$num  = $atts['number'];

	switch ( $atts['cols'] ) {
		case 1:
			$cols = 12;
			break;
		case 2:
			$cols = 6;
			break;
		case 3:
			$cols = 4;
			break;
		case 4:
			$cols = 3;
			break;
		case 6:
			$cols = 2;
			break;
		default:
			$cols = 0;
	}

	$styles = array(
		'grid' => array( 'grid', 'card', 'cards', 'columns', 'cols' ),
		'list' => array( 'list', 'lists', 'row', 'inline', 'rows' ),
	);
	$style  = $atts['style'];
	$style  = in_array( $style, $styles['grid'], true ) ? 'grid' : '';
	$style  = in_array( $style, $styles['list'], true ) ? 'list' : $style;

	if ( 'grid' === $style ) {
		$cols        = ( 1 === $cols ) ? 'py-3 col-12' : 'align-items-stretch d-flex py-3 col-12 col-sm-6 col-md-' . $cols;
		$wrap_cls    = 'card-deck';
		$item_cls    = 'card';
		$img_cls     = 'card-img-top';
		$body_cls    = 'card-body';
		$tit_cls     = 'card-title';
		$txt_cls     = 'card-text';
		$img_sz['w'] = 375;
		$img_sz['h'] = 225;
	} else {
		$wrap_cls    = 'list';
		$cols        = ( $cols >= 2 ) ? 'py-3 col-12 col-lg-6' : 'py-3 col-12';
		$item_cls    = 'media flex-fill shadow-sm';
		$img_cls     = 'mr-3 align-self-start';
		$body_cls    = 'media-body px-3';
		$tit_cls     = 'my-2';
		$txt_cls     = 'mb-2';
		$img_sz['w'] = 150;
		$img_sz['h'] = 150;
	}

	global $post;

	$my_query = new WP_Query(
		array(
			'post_type'      => $type,
			'posts_per_page' => $num,
			'orderby'        => 'date',
		)
	);

	$html  = '';
	$html .= '
	<section class="recents p-3">
		<h3 class="px-3 font-weight-normal mt-3 mb-1 text-primary">' . $text . '</h3>
		<div class="row m-0">
	';

	// LOOP
	if ( $my_query->have_posts() ) :
		while ( $my_query->have_posts() ) :
			$my_query->the_post();

			$dia = get_the_date( 'd' );
			$mes = get_the_date( 'M' );
			$ano = get_the_date( 'Y' );

			$thumb = ( has_post_thumbnail() ) ?
				odin_thumbnail( $img_sz['w'], $img_sz['h'], get_the_title(), true, $img_cls ) :
				'<img class="img-fluid ' . $img_cls . '" src="' . get_bloginfo( 'template_directory' ) . '/assets/img/nothumb.png" alt="' . __( 'No image', 'odin' ) . '"/>';

			$sticky = ( is_sticky() ) ? ' sticky' : '';

			$html .= '<div class="' . $cols . '">';
			$html .= '<article id="post-' . get_the_ID() . '" class="' . implode( ' ', get_post_class( $post->ID ) ) . $sticky . ' ' . $item_cls . ' align-items-center">
					' . $thumb . '
					<div class="' . $body_cls . '">
						<h4 class="h5 ' . $tit_cls . '"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h4>
						<p class="' . $txt_cls . '">' . odin_excerpt( 'excerpt', 10 ) . '</p>
						<p class="' . $txt_cls . '"><small class="text-black-50">
							<svg class="icon mr-1"><use xlink:href="#clock" /></svg>
							' . __( 'Posted', 'odin' ) . '
							<span class="entry-date">
							<time datetime="' . esc_attr( get_the_date( 'c' ) ) . '">' . esc_html( get_the_date() ) . '</time>
							</span>
							</small>
						</p>
					</div>
				</article>
			';
			$html .= '</div>';

		endwhile;
		wp_reset_postdata();

		$html .= '
			</div>
		</section>';
	endif;

	return $html;
}
add_shortcode( 'recents', 'odin_recent_post' );
