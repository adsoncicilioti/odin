<?php
/**
 * Google Maps shortcode.
 *
 * @param  array  $atts    Shortcode attributes.
 * @param  string $content Content.
 *
 * @return string          Google Maps HTML.
 */
function map( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'id'                => 'odin_map',
    'latitude'          => '0',
    'longitude'         => '0',
    'zoom'              => '10',
    'width'             => '600',
    'height'            => '400',
    'maptype'           => 'ROADMAP',
    'address'           => false,
    'kml'               => false,
    'kmlautofit'        => true,
    'marker'            => false,
    'markerimage'       => false,
    'traffic'           => false,
    'bike'              => false,
    'fusion'            => 'false',
    'infowindow'        => $infowindow,
    'infowindowdefault' => 'true',
    'color'             => '#005CB9',
    'hidecontrols'      => 'false',
    'drag'				=> 'true',
    'off2clickzoom'		=> 'false',
    'scale'             => 'false',
    'scrollwheel'       => 'true'
  ), $atts ) );

  if ( $infowindow ) {
        // First convert and decode html chars.
        $infowindow = str_replace('<br />', '<br>', $infowindow);
  }

  // JS var.
  $id = str_replace( '-', '_', $id );

  $html = '<div class="odin-map" id="' . $id . '" style="width: ' . $width . 'px; height: ' . $height . 'px;"></div>';

  $js = '<script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyC4RkH5BRTP56DoUCzmpRRMSEdJeCYnWXQ"></script>';
  $html .= apply_filters( 'odin_map_shortcode_js_' . $id, $js );
  $html .= '<script type="text/javascript">var styles =[
    {
        "featureType": "all",
        "elementType": "all",
        "stylers": [
            {
                "hue": "' . $color . '"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "hue": "' . $color . '"
            }
        ]
    }
];

var styledMap = new google.maps.StyledMapType(styles,{name:"Styled Map"});
var latlng = new google.maps.LatLng(' . $latitude . ', ' . $longitude . ');
var myOptions = {
  zoom: ' . $zoom . ',
  center: latlng,
  scrollwheel: ' . $scrollwheel .',
  scaleControl: ' . $scale .',
  disableDefaultUI: ' . $hidecontrols .',
  draggable: ' . $drag . ',
  disableDoubleClickZoom:' . $off2clickzoom . ',
  mapTypeId: google.maps.MapTypeId.' . $maptype . '
};
var ' . $id . ' = new google.maps.Map(document.getElementById("' . $id . '"), myOptions);
'.$id.'.mapTypes.set("map_style", styledMap);
'.$id.'.setMapTypeId("map_style");';

  // Kml.
  if ( $kml ) {
    if ( $kmlautofit ) {
      $html .= 'var kmlLayerOptions = {preserveViewport:true};';
    } else {
      $html .= 'var kmlLayerOptions = {preserveViewport:false};';
    }

    $html .= 'var kmllayer = new google.maps.KmlLayer("' . html_entity_decode( $kml ) . '", kmlLayerOptions);
      kmllayer.setMap(' . $id . ');';
  }

  // Traffic.
  if ( $traffic ) {
    $html .= 'var trafficLayer = new google.maps.TrafficLayer();trafficLayer.setMap(' . $id . ');';
  }

  // Bike.
  if ( $bike ) {
    $html .= 'var bikeLayer = new google.maps.BicyclingLayer();bikeLayer.setMap(' . $id . ');';
  }

  // Fusion tables.
  if ( $fusion ) {
    $html .= 'var fusionLayer = new google.maps.FusionTablesLayer(' . $fusion . ');fusionLayer.setMap(' . $id . ');';
  }

  // Address.
  if ( $address ) {
    $html .= 'var geocoder_' . $id . ' = new google.maps.Geocoder();var address = \'' . $address . '\';geocoder_' . $id . '.geocode( { \'address\': address}, function(results, status) { if (status == google.maps.GeocoderStatus.OK) {' . $id . '.setCenter(results[0].geometry.location);';

    if ( $marker ) {
      // Add custom image.
      if ( $markerimage ) {
        $html .= 'var image = "'. $markerimage .'";';
      }

      $html .= 'var marker = new google.maps.Marker({ map: ' . $id . ',';
      if ( $markerimage ) {
        $html .= 'icon: image,';
      }

      $html .= 'position: ' . $id . '.getCenter() });';

      // Infowindow
      if ( $infowindow ) {
        // First convert and decode html chars.
        $html .= '
          var contentString = \'' . $infowindow . '\';
          var infowindow = new google.maps.InfoWindow({content: contentString});google.maps.event.addListener(marker, \'click\', function() { infowindow.open(' . $id . ',marker);});';

        // Infowindow default
        if ( $infowindowdefault ) {
          $html .= 'infowindow.open(' . $id . ', marker);';
        }
      }
    }

    $html .= '} else { document.getElementById(' . $id . ').style.display = "block"; }});';
  }

  // Marker: show if address is not specified.
  if ( $marker && $address || $marker ) {
    // Add custom image.
    if ( $markerimage ) {
      $html .= 'var image = "'. $markerimage .'";';
    }

    $html .= 'var marker = new google.maps.Marker({ map: ' . $id . ',';

    if ( $markerimage ) {
      $html .= 'icon: image,';
    }

    $html .= 'position: ' . $id . '.getCenter()});';

    // Infowindow.
    if ( $infowindow ) {
      $html .= 'var contentString = \'' . $infowindow . '\';var infowindow = new google.maps.InfoWindow({content: contentString});google.maps.event.addListener(marker, \'click\', function() {infowindow.open(' . $id . ',marker);});';

      // Infowindow default
      if ( $infowindowdefault ) {
        $html .= 'infowindow.open(' . $id . ',marker);';
      }
    }
  }

  $html .= '</script>';

  return $html;
}
add_shortcode('maps', 'map');
