<?php
// MINI REGISTER
function odin_mini_register( $atts, $html = '' ) {

	extract(
		shortcode_atts(
			array(
				'class_form'     => '',
				'class_name'     => '',
				'input_name'     => '',
				'class_email'    => '',
				'input_email'    => '',
				'class_btn'      => 'btn-primary',
				'class_btn_wrap' => '',
				'btn_txt'        => 'Inscreva-me',
			),
			$atts
		)
	);

	global $post;

	$html = '<form method="post" action="' . site_url( 'wp-login.php?action=register', 'login_post' ) . '" class="wp-user-form ' . $class_form . '">
	<div class="form-group mb-1 ' . $class_name . '">
		<label class="sr-only" for="f_user_login">' . __( 'Nome de Usuário', 'odin' ) . ': </label>
		<input class="form-control ' . $input_name . '" type="text" name="user_login" value="" id="f_user_login" required="required" placeholder="' . __( 'Seu Nome', 'odin' ) . '"/>
	</div>
	<div class="form-group mb-1 ' . $class_email . '">
		<label class="sr-only" for="f_user_email"> ' . __( 'E-mail', 'odin' ) . ': </label>
		<input class="form-control ' . $input_email . '" type="email" name="user_email" value="" id="f_user_email" required="required" placeholder="' . __( 'Seu email', 'odin' ) . '" />
			' . do_action( 'register_form' ) . '
	</div>
	<div class="form-group mb-1 ' . $class_btn_wrap . '">
		<button class="btn ' . $class_btn . ' mb-1" type="submit" name="user-submit">' . __( $btn_txt, 'odin' ) . ' <i class="ico-send"></i></button>
		<input type="hidden" name="redirect_to" value="' . $_SERVER['REQUEST_URI'] . '?register=true" />
		<input type="hidden" name="user-cookie" value="1" />
	</div>
	</form>';

	return $html;
}
add_shortcode( 'mini_register', 'odin_mini_register' );
