<?php

function blocks_home_cpt() {
    $blockshome = new Odin_Post_Type(
        __( 'Block', 'odin' ), // Nome (Singular) do Post Type.
        'block-home' // Slug do Post Type.
    );

    $blockshome->set_labels(
        array(
            'menu_name' => __( 'Home Blocks', 'odin' )
        )
    );

    $blockshome->set_arguments(
        array(
            'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
            'exclude_from_search' => true,
            'menu_icon' => 'dashicons-exerpt-view'
        )
    );
}

add_action( 'init', 'blocks_home_cpt', 1 );

/////// Meta BOXES

function blocks_home_mtbx() {

  global $post;

    $prefix = '_blchm_';

    $blocks_mtbx = new Odin_Metabox(
        $prefix . 'blocks', // Slug/ID of the Metabox (Required)
        __('Opções do Bloco', 'odin'), // Metabox name (Required)
        'block-home', // Slug of Post Type (Optional)
        'normal', // Context (options: normal, advanced, or side) (Optional)
        'high' // Priority (options: high, core, default or low) (Optional)
    );

    $blocks_mtbx->set_fields(

        array(

            array(
                'id'   => $prefix . 'ftimag_title', // Obrigatório
                'label'=> __( 'Opções da Imagem Destacada', 'odin' ), // Obrigatório
                'type' => 'title', // Obrigatório
            ),

            // Align Featured Image.
            array(
                'id'          => $prefix . 'ftimg_align', // Required
                'label'       => __( 'Alinhament da Imagem Destacada', 'odin' ), // Required
                'type'        => 'radio', // Required
                // 'attributes' => array(), // Optional (html input elements)
                'default'    => 'center-block txt-c', // Optional
                'description' => __( 'Defia o alinhamento da imagem Destacada', 'odin' ), // Optional
                'options' => array( // Required (id => title)
                  'float-md-left txt-c mb-3'   => __('Esquerda', 'odin'),
                  'center-block txt-c'   => __('Centro', 'odin'),
                  'float-md-right txt-c mb-3' => __('Direita', 'odin'),
                  'float-md-left txt-c l-out sr-l'   => __('flutuar à Esquerda', 'odin'),
                  'float-md-right txt-c r-out sr-r' => __('flutuar à Direita', 'odin')
                ),
            ),

            // Style Featured Image.
            array(
                'id'          => $prefix . 'ftimg_style', // Required
                'label'       => __( 'Estilo da Imagem Destacada', 'odin' ), // Required
                'type'        => 'radio', // Required
                // 'attributes' => array(), // Optional (html input elements)
                'default'    => ' ', // Optional
                'description' => __( 'Defina o estilo da imagem Destacada', 'odin' ), // Optional
                'options' => array( // Required (id => title)
                  ' '   => __('Normal', 'odin'),
                  'img-rounded'   => __('Cantos arredondados', 'odin'),
                  'img-circle' => __('Círculo <small>(melhor para imagens com a mesma altura e largura)</small>', 'odin')
                ),
            ),

            array(
                'id'   => $prefix . 'sep-1', // Obrigatório
                'type' => 'separator' // Obrigatório
            ),


            array(
                'id'   => $prefix . 'head_title', // Obrigatório
                'label'=> __( 'Opções do Títilo & Descrição', 'odin' ), // Obrigatório
                'type' => 'title', // Obrigatório
            ),

            // Align Title & Description Radio field.
            array(
                'id'          => $prefix . 'title_align', // Required
                'label'       => __( 'Alinhamento do Títilo & Descrição', 'odin' ), // Required
                'type'        => 'radio', // Required
                // 'attributes' => array(), // Optional (html input elements)
                'default'    => 'txt-c', // Optional
                'description' => __( 'Defina um alinhamento para o Títilo & Descrição.', 'odin' ), // Optional
                'options' => array( // Required (id => title)
                    'txt-md-l'   => __('Esquerda', 'odin'),
                    'txt-md-c'   => __('Centro', 'odin'),
                    'txt-md-r' => __('Direita', 'odin')
                ),
            ),

            // Light Title radio field.
            array(
                'id'          => $prefix . 'title_hide', // Required
                'label'       => __( 'Ocultar Título', 'odin' ), // Required
                'type'        => 'radio', // Required
                'default'    => ' ',
                // 'attributes' => array(), // Optional (html input elements)
                'description' => __( 'Chose to Show or Hide the Title text.', 'odin' ), // Optional
                'options' => array( // Required (id => title)
                    ' '   => __('Mostrar <small>(normal)</small>', 'odin'),
                    'sr-only'   => __('Ocultar', 'odin')
                ),
            ),

            // Descripton
            array(
                'id'          => $prefix . 'desc', // Obrigatório
                'label'       => __( 'Descrição do Bloco', 'odin' ), // Obrigatório
                'type'        => 'editor', // Required
                'description' => __( 'Adicione uma breve descrição a este Bloco.', 'odin' ), // Optional
                'options'     => array( // Optional
                    'textarea_rows' => 4,
                    'teeny' => true,
                    'quicktags' => false,
                    'media_buttons' => false,
                    'wpautop' => false
                ),
            ),

            // Title & Description Color field.
            array(
                'id'          => $prefix . 'head_color', // Required
                'label'       => __( 'Cor dos textos do Títilo & Descrição', 'odin' ), // Required
                'type'        => 'radio', // Required
                // 'default'     => '#3f3b46',
                // 'attributes' => array(), // Optional (html input elements)
                'description' => __( 'Atribua uma cor para os Textos do Título e Descrição.', 'odin' ), // Optional
                'options' => array(
                  'text-white' => __('Branco', 'odin'),
                  'text-light' => __('Cinza Claro', 'odin'),
                  'text-muted' => __('Cinza', 'odin'),
                  'text-dark' => __('Negro', 'odin'),
                  'text-success' => __('Verde', 'odin'),
                  'text-primary' => __('Azul', 'odin'),
                  'text-info' => __('Ciano', 'odin'),
                  'text-warning' => __('Amarelo', 'odin'),
                  'text-danger' => __('Vermelho', 'odin')
                ),
            ),

            array(
                'id'          => $prefix . 'cont_color', // Required
                'label'       => __( 'Cor dos textos do Conteúdo', 'odin' ), // Required
                'type'        => 'radio', // Required
                // 'default'     => '#3f3b46',
                // 'attributes' => array(), // Optional (html input elements)
                'description' => __( 'Atribua uma cor para os Textos do Conteúdo do Bloco.', 'odin' ), // Optional
                'options' => array(
                  'text-white' => __('Branco', 'odin'),
                  'text-light' => __('Cinza Claro', 'odin'),
                  'text-muted' => __('Cinza', 'odin'),
                  'text-dark' => __('Negro', 'odin'),
                  'text-success' => __('Verde', 'odin'),
                  'text-primary' => __('Azul', 'odin'),
                  'text-info' => __('Ciano', 'odin'),
                  'text-warning' => __('Amarelo', 'odin'),
                  'text-danger' => __('Vermelho', 'odin')
                ),
            ),

            array(
                'id'   => $prefix . 'sep-2', // Obrigatório
                'type' => 'separator' // Obrigatório
            ),

            array(
                'id'   => $prefix . 'bg_title', // Obrigatório
                'label'=> __( 'Opções do Plano de Fundo', 'odin' ), // Obrigatório
                'type' => 'title', // Obrigatório
            ),

            // BG Color field.
            array(
                'id'          => $prefix . 'colorbg', // Required
                'label'       => __( 'Cor do Plano de Fundo', 'odin' ), // Required
                'type'        => 'color', // Required
                // 'attributes' => array(), // Optional (html input elements)
                // 'default'     => '', // Optional (color in hex)
                'description' => __( 'Escolha uma cor para o Plano de Fundo.', 'odin' ), // Optional

            ),

            // BG Image Upload field.
            array(
                'id'          => $prefix . 'bgimg', // Required
                'label'       => __( 'Imagem de Fundo do Bloco', 'odin' ), // Required
                'type'        => 'image', // Required
                // 'default'     => '', // Optional (image attachment id)
                'description' => __( 'Defina uma imagem para aparecer no plano de fundo.', 'odin' ), // Optional
            ),

            // BG Image size/Cover
            array(
                'id'            => $prefix . 'bgcover', // Obrigatório
                'label'         => __( 'Comportamento da Imagem de Fundo', 'odin' ), // Obrigatório
                'type'          => 'radio', // Obrigatório
                // 'attributes' => array(), // Opcional (atributos para input HTML/HTML5)
                // 'default'       => '', // Opcional
                'options'       => array( // Obrigatório (adicione aque os ids e títulos)
                    'cover'   => __( 'Cobrir tudo', 'odin' ),
                    'contain'   => __( 'Encaixar', 'odin' ),
                    'auto' => __( 'Tamanho original', 'odin' )
                ),
            ),
            // BG Image Position
            array(
                'id'            => $prefix . 'bgposit', // Obrigatório
                'label'         => __( 'Background image Position', 'odin' ), // Obrigatório
                'type'          => 'radio', // Obrigatório
                // 'attributes' => array(), // Opcional (atributos para input HTML/HTML5)
                // 'default'       => ' ', // Opcional
                'options'       => array( // Obrigatório (adicione aque os ids e títulos)
                    'left center'   => __( 'Esquerda', 'odin' ),
                    '25% center'   => __( '25% da Esquerda', 'odin' ),
                    'center center' => __( 'Centro', 'odin' ),
                    '75% center'   => __( '25% da Direita', 'odin' ),
                    'right center' => __( 'Direita', 'odin' )
                ),
            ),

            // BG Image Repeat Select field.
            array(
                'id'            => $prefix . 'bgrepeat', // Obrigatório
                'label'         => __( 'Repetição da Imagem de fundo', 'odin' ), // Obrigatório
                'type'          => 'radio', // Obrigatório
                // 'attributes' => array(), // Opcional (atributos para input HTML/HTML5)
                //'default'       => '', // Opcional
                'options'       => array( // Obrigatório (adicione aque os ids e títulos)
                    'no-repeat'   => __( 'Não repetir', 'odin' ),
                    'repeat'   => __( 'Repetir para todos os lados', 'odin' ),
                    'repeat-x' => __( 'Repetit horizontalmente', 'odin' ),
                    'repeat-y'   => __( 'Repetir Verticalmente', 'odin' )
                ),
            ),
            array(
                'id'   => $prefix . 'sep-3', // Obrigatório
                'type' => 'separator' // Obrigatório
            ),

            array(
                'id'   => $prefix . 'block_title', // Obrigatório
                'label'=> __( 'Opções do Bloco', 'odin' ), // Obrigatório
                'type' => 'title', // Obrigatório
            ),

            // Block ID.

            array(
                'id'          => $prefix . 'id', // Required
                'label'       => __( 'Defina uma ID para este bloco', 'odin' ), // Required
                'type'        => 'text', // Required
                // 'attributes' => array(), // Optional (html input elements)
                // 'default'     => the_ID(),
                'description' => __( 'Use apenas letras minúsculas sem espaço', 'odin' ), // Optional
            ),
            // Animação.
            array(
                'id'          => $prefix . 'animate', // Required
                'label'       => __( 'Animação do Bloco', 'odin' ), // Required
                'type'        => 'radio', // Required
                // 'attributes' => array(), // Optional (html input elements)
                'default'    => ' ', // Optional
                'description' => __( 'Defina uma animação de entrada para o Bloco.', 'odin' ), // Optional
                'options' => array( // Required (id => title)
                  ' '   => __('Sem animação', 'odin'),
                  'sr-t'   => __('Do Topo', 'odin'),
                  'sr-r'   => __('Da direita', 'odin'),
                  'sr-b'   => __('De Baixo', 'odin'),
                  'sr-l'   => __('Da esquerda', 'odin'),
                  'sr-fade'   => __('Esmaecer', 'odin')
                ),
            ),

            // Divisores
            array(
                'id'          => $prefix . 'divsvg', // Required
                'label'       => __( 'Divisores do Bloco', 'odin' ), // Required
                'type'        => 'radio', // Required
                // 'attributes' => array(), // Optional (html input elements)
                'default'    => '', // Optional
                'description' => __( 'defina se o bloco possuirá Divisores', 'odin' ), // Optional
                'options' => array( // Required (id => title)
                  ''   => __('Nenhum', 'odin'),
                  'div-top'   => __('Topo', 'odin'),
                  'div-down'   => __('Baixo', 'odin'),
                  'div-two'   => __('Ambos', 'odin'),
                  'empty-top' => __('Espaço no Topo', 'odin'),
                  'empty-down' => __('Espaço em Baixo', 'odin'),
                  'empty-two' => __('Espaço em Ambos', 'odin')
                ),
            ),

        )
    );
}

add_action( 'init', 'blocks_home_mtbx', 1 );

////////////// Display blocks Home

function new_blocks_home() { //call this function where you want to display the blocks in your theme.

    $prefix = '_blchm_';
    $fits = array('post_type' => 'block-home', 'posts_per_page' => 1);
    //if there is 1 block show the blocks...
    if($fits){

      global $post;
      //Limit the blocks to 5 blocks
      $args = array('post_type' => 'block-home', 'posts_per_page' => 999, 'order' => 'ASC', 'orderby'   => 'menu_order');
      $loop = new WP_Query($args);
      while ($loop->have_posts()) : $loop->the_post();

      $get_bgcover = get_post_meta( $post->ID, $prefix . 'bgcover', true );
      if (isset($get_bgcover)) {
          $bgcover = 'background-size:'.$get_bgcover.';';
      }

      $get_bgposit = get_post_meta( $post->ID, $prefix . 'bgposit', true );
      if ( $get_bgposit ) {
        $bgposit = 'background-position: '.$get_bgposit.';';
      }

      $get_bgrepeat = get_post_meta( $post->ID, $prefix . 'bgrepeat', true );
      if ( $get_bgrepeat ) {
        $bgrepeat = 'background-repeat: '.$get_bgrepeat.';';
      }

      $get_block_ID = get_post_meta( $post->ID, $prefix . 'id', true );
      if ($get_block_ID) {
          $block_ID = 'id="'.$get_block_ID.'"';
      } else {
          $block_ID = 'id="'.get_the_ID().'"';
      }

      $get_animate = get_post_meta( $post->ID, $prefix . 'animate', true );
      if ($get_animate) {
          $animate = $get_animate;
      }

      $colorbg =  get_post_meta( $post->ID, $prefix . 'colorbg', true );
      if (isset($colorbg)) {
        $color_bg = 'background-color: '.$colorbg.';';
        $div_color = 'fill: '.$colorbg.';';
      }

      $get_divsvg = get_post_meta( $post->ID, $prefix . 'divsvg', true );
      $divtop = '<svg class="absolute z3 div-top div-'.get_the_id().'" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="90" viewBox="0 0 1170 90" preserveAspectRatio="none"><path d="M1170 90V0L600.76 89a118.9 118.9 0 0 1-30.56 0L1 0H0v90z"/></svg>';
      $divdown = '<svg class="absolute z3 div-down div-'.get_the_id().'" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="90" viewBox="0 0 1170 90" preserveAspectRatio="none"><path d="M0 0l569.7 89.12A119.16 119.16 0 0 0 600.3 89L1170 0H0z"/></svg>';
      $emptytop = '<div class="center-block empty-top  empty-'.get_the_id().'"></div>';
      $emptydown = '<div class="center-block empty-down  empty-'.get_the_id().'"></div>';

      if ( $get_divsvg == 'div-top' ) {
        $div_t = $divtop;
        $div_d = '';
        $divz = '';
      } elseif ( $get_divsvg == 'div-down' ) {
        $div_t = '';
        $div_d = $divdown;
        $divz = 'z1';
      } elseif ($get_divsvg == 'div-two') {
        $div_t = $divtop;
        $div_d = $divdown;
        $divz = 'z1';
      } elseif ($get_divsvg == 'empty-top') {
        $div_t = $emptytop;
        $div_d = '';
        $divz = '';
      } elseif ($get_divsvg == 'empty-down') {
        $div_t = '';
        $divz = '';
        $div_d = $emptydown;
      } elseif ($get_divsvg == 'empty-two') {
        $div_t = $emptytop;
        $div_d = $emptydown;
        $divz = '';
      } else {
        $div_t = '';
        $div_d = '';
        $divz = '';
      }

      $cont_color =  get_post_meta( $post->ID, $prefix . 'cont_color', true );
      echo '<section ', $block_ID, ' class="'.$divz.' relative ',$cont_color,' cf p-y-3 home-block ', $animate , ' block-', the_id(), '">';

      $bg_img_id = get_post_meta( $post->ID, $prefix . 'bgimg', true );
      $bg_img_url = wp_get_attachment_image_src( $bg_img_id, 'large', false);
      if ($bg_img_url) {
          if ($bg_img_url) (string)$bg_img_url = 'background-image: url(' . $bg_img_url[0] . ');';
      }

      $head_color = get_post_meta( $post->ID, $prefix . 'head_color', true );

      //STYLE SCOPED

      if ( !empty($color_bg) || !empty($bg_img_url) || !empty($bgrepeat) || !empty($bgcover) || !empty($bgposit) ) {
        echo '<style type="text/css" scoped>
          .block-'.get_the_id().' {';
            if (!empty($colorbg) ) { echo $color_bg; };
            if (isset($bg_img_url)) { echo $bg_img_url; };
            if (isset($bgrepeat)) { echo $bgrepeat; };
            if (!empty($get_bgcover)) { echo $bgcover; };
            if (isset($bgposit)) { echo $bgposit; };
          echo '}';
          if ( strstr($get_divsvg, 'div')) {
            echo '.div-'.get_the_id().' {';
              if (isset($div_color)) { echo $div_color; };
              echo '
              left: 0;
            }';
          };
          if ($div_t == $divtop) {
            echo '.div-'.get_the_id().'.div-top {';
              echo 'top: -90px;
            }';
          };
          if ($div_d == $divdown) {
            echo '.div-'.get_the_id().'.div-down {';
              echo 'bottom: -90px;
            }';
          };
          if (strstr($get_divsvg, 'empty')) {
            echo '.empty-'.get_the_id().' {';
              echo 'height: 90px;
            }';
          };

          echo '</style>';
      } else {
        echo '';
      };
      echo $div_t;
      echo '<div class="p-3 clearfix "> ';


      $ftimg_align = get_post_meta( $post->ID, $prefix . 'ftimg_align', true );
      $ftimg_style = get_post_meta( $post->ID, $prefix . 'ftimg_style', true );

      if ( has_post_thumbnail() && $ftimg_align != 'center-block txt-c') {
        $col = 'col-md-6';
        $col_content = 'class="col-md-6"';
      } else {
        $col = '';
        $col_content = '';
      }

      if ( has_post_thumbnail() ) {
        echo '<figure class="'.$ftimg_align.' px-2 '.$col.'">';
        the_post_thumbnail('full', array( 'class' => $ftimg_style ));
        echo '</figure>';
      }

      $title_align = get_post_meta( $post->ID, $prefix . 'title_align', true );

      echo '<div '.$col_content.'>
      <header class="p-0 ', $title_align,' ',$head_color,'">';
        $title_hide = get_post_meta( $post->ID, $prefix . 'title_hide', true );

        echo '<h2 class="block-title h1 display-3 ', $title_hide, '">', the_title(), '</h2>'; //block Big Title

        $desc = get_post_meta( $post->ID, $prefix . 'desc', true );
        if (!empty($desc)) {
          echo '<div class="lead mb-3">',$desc,'</div>';
        } else {
          //nothing
        }

      echo '</header>',
      the_content(),
      '</div>
      </div><!-- .clearfix -->';
      echo $div_d;
      wp_reset_postdata();
      echo '</section>';// block Item
      endwhile;
    };
};

////////////*** Hide editor for specific page templates. */
add_action('init', 'remove_editor_init');
function remove_editor_init() {
    // if post not set, just return
    // fix when post not set, throws PHP's undefined index warning
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } else if (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    } else {
        return;
    }
    $template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
    if ($template_file == 'page-home.php') {
        remove_post_type_support('page', 'editor');
    }
}

add_action('add_meta_boxes', 'add_info_blocks_home');
function add_info_blocks_home() {
    global $post;

    if(!empty($post))
    {
        $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

        if($pageTemplate == 'page-home.php' )
        {
            add_meta_box(
                'info_blocks_home', // $id
                __('Home page co Blocos', 'odin'), // $title
                'show_info_blocks_home', // $callback
                'page', // $page
                'normal', // $context
                'high'); // $priority
        }
    }
}

function show_info_blocks_home() {
    // Add the HTML for the post meta
    echo '
    <h2>'; echo __('Adicione conteúdo com o menu <b>Home Blocks</b>','Odin') . '</h2>
    <ol>
        <li>'; echo __('Vá até o menu <b>Blocks Home</b> na barra lateral à esquerda, ou clique <a href="', 'odin') . admin_url( 'edit.php?post_type=block-home') . __('">AQUI</a>','Odin') . '</li>
        <li>'; echo __('Clique no botão <b>Adicionar Novo</b> para criar um novo Bloco de conteúdo para sua Home Page','Odin') . '</li>
        <li>'; echo __('Para alterar a ordem dos Blocos, acesse a lista de Blocos criados: <a href="', 'odin') . admin_url( 'edit.php?post_type=block-home') . __('">Todos os Blocos</a>, Escolha um e clique em Edição rápida. No campo ordem, defina um numero (a partir do 0). Quanto menor o valor mais ao topo o bloco vai aarecer.', 'odin') . '</li>
    </ol>';
}

/////////////// COLUNA COM MINIATURA

add_filter( 'manage_block-home_posts_columns', 'set_block_thumb_col', 5 );
add_action( 'manage_block-home_posts_custom_column' , 'block_thumb_col', 10, 2 );

function set_block_thumb_col($columns) {

  $columns[ 'block_thumbnail' ] = __( 'Block Image', 'odin' );

    return $columns;
};

function block_thumb_col( $thumbnail ) {
  global $post;
    if ( has_post_thumbnail() ) {
        $thumbnail = odin_thumbnail( 80, 80, 'Miniatura do Bloco', true, 'img-fluid' );
        echo $thumbnail;
    } else {
        echo '<img width="80" height="80" src="', get_template_directory_uri(),'/assets/images/nothumb.jpg" alt="', __('No image', 'odin'), '">';
    }
};
