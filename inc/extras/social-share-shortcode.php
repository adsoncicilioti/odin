<?php
/*********************
SOCIAL SHARE
*********************/

add_filter( 'the_content', 'social_share_btns');

function social_share_btns( $html = '' ) {

	// Get current page URL
	$odinURL = urlencode( wp_get_shortlink() );

	// Twitter VIA @
	// $twtprofile = '';

	// Get current page title
	$odinTitle = str_replace( ' ', '%20', get_the_title());

	// Construct sharing URL without using any script
	$twitterURL = 'https://twitter.com/intent/tweet?text='.$odinTitle.'&amp;url='.$odinURL/*.'&amp;via='.$twtprofile*/;
	$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$odinURL;
	$googleURL = 'https://plus.google.com/share?url='.$odinURL;
	// $bufferURL = 'https://bufferapp.com/add?url='.$odinURL.'&amp;text='.$odinTitle;
	$whatsappURL = 'whatsapp://send?text='.$odinTitle . ' ' . $odinURL;
	$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$odinURL.'&amp;title='.$odinTitle;

	// Based on popular demand added Pinterest too
	//$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$odinURL.'&amp;media='.$odinThumbnail[0].'&amp;description='.$odinTitle;


	if ( is_single() || is_product() ) {
        // Add sharing button at the end of page/page html
		$html .= '<div class="odin-social mb-2">';
		$html .= '<hr class="my-2" /><h5 class="font-bold">Compartilhe pelo</h5> <div class="btn-group"><a class="m-0 share-link share-facebook btn btn-outline-blue" href="'.$facebookURL.'" target="_new" title="Facebook"><svg class="icon"><use xlink:href="#facebook" /></svg><span class="sr-only">Facebook</span></a>';
		$html .= '<a class="m-0 share-link share-twitter btn btn-outline-info" href="'. $twitterURL .'" target="_new" title="Twitter"><svg class="icon"><use xlink:href="#twitter" /></svg> <span class="sr-only">Twitter</span></a>';
		$html .= '<a class="m-0 share-link share-whatsapp d-md-none btn btn-outline-success" href="'.$whatsappURL.'" target="_new" title="WhatsApp"><svg class="icon"><use xlink:href="#whatsapp" /></svg> <span class="sr-only">WhatsApp</span></a>';
		$html .= '<a class="m-0 share-link share-googleplus btn btn-outline-red" href="'.$googleURL.'" target="_new" title="Google+"><svg class="icon"><use xlink:href="#google-plus" /></svg> <span class="sr-only">Google+</span></a>';
		//$html .= '<a class="share-link share-buffer btn btn-outline-" href="'.$bufferURL.'" target="_new">Buffer</a>';
		$html .= '<a class="m-0 share-link share-linkedin btn btn-outline-dark" href="'.$linkedInURL.'" target="_new" title="LinkedIn"><svg class="icon"><use xlink:href="#linkedin" /></svg> <span class="sr-only">LinkedIn</span></a>';
		$html .= '<a class="m-0 share-link share-email btn btn-secondary" href="mailto:?subject=Confira%20este%20post:%20'.$odinTitle.'&amp;body=%20Artigo%20completo%20em:%20'.$odinURL.'"title="E-mail" target="_new"><svg class="icon"><use xlink:href="#mail" /></svg> <span class="sr-only">E-mail</span></a>';
		//$html .= '<a class="share-link share-pinterest" href="'.$pinterestURL.'" target="_new">Pin It</a>';
		$html .= '</div></div>';

    }
           return $html;

}


// // REMOVE FROM EXCERPT
function ignore_social_filter($html) {
    if (has_filter( 'the_content', 'social_share_btns' )) {
        remove_filter( 'the_content', 'social_share_btns' );
    }
    return $html;
}
add_filter('get_the_excerpt', 'ignore_social_filter', 9);

function re_add_social_filter($html) {
    add_filter( 'the_content', 'social_share_btns' ); // if this filter got priority different from 10 (default), you need to specify it
    return $html;
}
add_filter('get_the_excerpt', 're_add_social_filter', 11); //priority needs to be higher than that of wp_trim_excerpt, which has priority of 10.
