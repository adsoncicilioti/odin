<?php
/**
 * The Shortcode for displaying Call to Search.
 *
 * @package Odin
 * @since 2.2.0
 */


// Add Shortcode
function call_to_search( $atts, $html = '' ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'text'  => '',
			'class' => '',
		),
		$atts
	);
	$text = $atts['text'];
	$text = ( $text ) ? $text : esc_html__( 'What are you looking for today?', 'odin' );

	$html = '
	<form method="get" class="justify-content-center align-items-center p-3 row m-0" action="' . esc_url( home_url( '/' ) ) . '" role="search">
		<div class="col-xs-12 col-md py-3">
			<h3 class="h5 mb-0 text-primary">' . $text . '</h3>
		</div>
		<div class="input-group col-xs-12 col-md-8 py-3">
			<input type="search" class="form-control" name="s" value="' . get_search_query() . '" placeholder="' . esc_attr__( 'Enter your search here and press enter.', 'odin' ) . '" required="required">
			<span class="input-group-append">
				<button type="submit" class="align-self-start btn btn-primary">
					<span>' . esc_html__( 'Search', 'odin' ) . '</span>
					<svg class="ml-md-2 mb-0 icon text-black-50 h4">
						<use xlink:href="#search" />
					</svg>
				</button>
			</span>
		</div>
	</form>';
	return $html;

}

add_shortcode( 'call_to_search', 'call_to_search' );
