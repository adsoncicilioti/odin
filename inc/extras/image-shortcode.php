<?php

// SHORTCODE TO INSERT IMAGES IN WIDGETS
// use [image] in pages or widgets
function image_shorcode($atts, $content = null){

	extract( shortcode_atts( array(
		'id' => '',
		'class' => 'alignright',
		'size' => 'thumbnail',
		'alt' => '',
	), $atts ) );

	$content = '<img src="' . wp_get_attachment_image_src( $id, $size )[0] . '" class="' . $class . '" alt="' . $alt . '" />';


	return $content;
}
add_shortcode('image', 'image_shorcode');
