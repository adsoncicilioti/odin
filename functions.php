<?php
/**
 * Functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * For documentation Odin Framework
 *
 * @link https://github.com/wpbrasil/odin
 *
 * For more information on hooks, actions, and filters,
 * @link https://codex.wordpress.org/Plugin_API
 *
 * @package odin
 */

/**
 * Classes.
 */

require_once get_template_directory() . '/inc/classes/abstracts/abstract-front-end-form.php';
// require_once get_template_directory() . '/inc/classes/class-bootstrap-nav.php';

/**
 * Megamenu
 */
require_once get_template_directory() . '/inc/megamenu/walkernav.php';
require_once get_template_directory() . '/inc/megamenu/megamenu-custom-fields.php';
require_once get_template_directory() . '/inc/classes/class-thumbnail-resizer.php';
// require_once get_template_directory() . '/inc/classes/class-theme-options.php';
// require_once get_template_directory() . '/inc/classes/class-options-helper.php';
// require_once get_template_directory() . '/inc/classes/class-post-type.php';
// require_once get_template_directory() . '/inc/classes/class-taxonomy.php';
// require_once get_template_directory() . '/inc/classes/class-metabox.php';
require_once get_template_directory() . '/inc/classes/class-contact-form.php';
// require_once get_template_directory() . '/inc/classes/class-post-form.php';
require_once get_template_directory() . '/inc/classes/class-user-meta.php';
// require_once get_template_directory() . '/inc/classes/class-post-status.php';
// require_once get_template_directory() . '/inc/classes/class-term-meta.php';

/**
 * Shortcodes.
 */

// require_once get_template_directory() . '/inc/shortcodes/class-odin-shortcodes.php';
// require_once get_template_directory() . '/inc/shortcodes/class-odin-shortcodes-menu.php';


/**
 * Widgets.
 */

// Facebook like box widget.
// require_once get_template_directory() . '/inc/widgets/class-odin-widget-like-box.php';

/**
 * Hooks.
 */


// Theme support options.
require_once get_template_directory() . '/inc/theme-support.php';

// WP Head and other cleanup functions.
require_once get_template_directory() . '/inc/cleanup.php';

// Register scripts and stylesheets.
require_once get_template_directory() . '/inc/enqueue-scripts.php';

// Register custom menus and menu walkers.
require_once get_template_directory() . '/inc/menu.php';

// Register sidebars/widget areas.
require_once get_template_directory() . '/inc/sidebar.php';

// Customize the WordPress admin and login menu.
require_once get_template_directory() . '/inc/admin.php';

// Automatically sets the post thumbnail.
require_once get_template_directory() . '/inc/autoset-featured.php';

/**
 * TGMPA
 */
require_once get_template_directory() . '/inc/tgmpa/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/inc/config-required-plugins.php';

/**
 * Odin Blocks - Lazy Blocks plugin
 */
require_once get_template_directory() . '/inc/odin-blocks/blocks.php';

/**
 * Helpers.
 */

// Custom templates tags.
require_once get_template_directory() . '/inc/template-tags.php';

// Custom comments loop.
require_once get_template_directory() . '/inc/comments-loop.php';

// Replace 'older/newer' post links with numbered navigation.
require_once get_template_directory() . '/inc/pagination.php';

// WooCommerce compatibility files.
require_once get_template_directory() . '/inc/woocommerce.php';

// Post functions.
require_once get_template_directory() . '/inc/post.php';

// Breadcrumbs function - no need to rely on plugins.
require_once get_template_directory() . '/inc/breadcrumbs.php';

// ======================= CUSTOMIZER ETUP ======================== //
if ( class_exists( 'Kirki' ) ) {
	require_once get_template_directory() . '/inc/odin-customizer/customizer-setup.php';
}

// HOME SLIDER
// require_once get_template_directory() . '/inc/extras/slider-home.php';
// HOME BLOCKS
// require_once get_template_directory() . '/inc/extras/blocks-home.php';
// Portfolio
// require_once get_template_directory() . '/inc/extras/portfolio.php';

// Contact form-control
require_once get_template_directory() . '/inc/extras/contact-form.php';

// MAPS
require_once get_template_directory() . '/inc/extras/maps-shortcode.php';

// IMAGE
require_once get_template_directory() . '/inc/extras/image-shortcode.php';

// BIZAGE
require_once get_template_directory() . '/inc/extras/bizage-shortcode.php';

// Call-to-search
require_once get_template_directory() . '/inc/extras/call-search-shortcode.php';


// WOOCOMMERCE HACKS
// if ( is_woocommerce_activated()) {
// require_once get_template_directory() . '/inc/extras/woocommerce-hacks.php';
// }
// MINI REGISTES
if ( is_woocommerce_activated() ) {
	require_once get_template_directory() . '/inc/extras/mini-register-shortcode.php';
}

// RECENT POSTS
require_once get_template_directory() . '/inc/extras/recent-posts-shortcode.php';


// SOCIAL SHARE
if ( is_woocommerce_activated() ) {
	require_once get_template_directory() . '/inc/extras/social-share-shortcode.php';
}




// MY HACKS
// Bootstrap v4 nav-item class
// add_filter('nav_menu_css_class' , 'a_navitem_class' , 10 , 2);
// function special_nav_class($classes, $item){
// $classes[] = "nav-item";
// return $classes;
// }
//
// function add_navlink_class($ulclass) {
// return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
// }
// add_filter('wp_nav_menu','add_navlink_class');
// Get User Role Name
function get_user_role() {
	global $wp_roles;
	$current_user = wp_get_current_user();
	$roles        = $current_user->roles;
	$role         = array_shift( $roles );
	return isset( $wp_roles->role_names[ $role ] ) ? translate_user_role( $wp_roles->role_names[ $role ] ) : false;
}

// Add AVATAR Shortcode
function odin_avatar( $avatar = '' ) {
	global $current_user;
	$avatar = get_avatar( $current_user->ID, 150, '', $current_user->display_name );

	return $avatar;
}
add_shortcode( 'get_avatar', 'odin_avatar' );

/**
 * Disable WordPress Admin Bar for all users but admins.
 */
add_action( 'after_setup_theme', 'remove_admin_bar' );
function remove_admin_bar() {
	show_admin_bar( false );
}

/**
 * ALLOWED HTML TAGS IN WP_EDITOR
 */
function override_mce_options( $initArray ) {
	$opts                                   = '*[*]';
	$initArray['paste_word_valid_elements'] = $opts;
	$initArray['valid_elements']            = $opts;
	$initArray['extended_valid_elements']   = $opts;
	return $initArray;
}
add_filter( 'tiny_mce_before_init', 'override_mce_options' );

/**
 *  Page Slug Body Class
 */
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

/**
 * REMOVE PLUGINS STYLE
 */
function remove_stylesheets() {
	if ( ! wp_style_is( 'wsl-widget', 'registered' ) ) {
		wp_deregister_style( 'wsl-widget' );
	}

	wp_dequeue_style( 'wsl-widget' );
}
add_action( 'wp_enqueue_scripts', 'remove_stylesheets' );
add_action( 'login_enqueue_scripts', 'remove_stylesheets' );


/**
 * CHANGE CLASS THE CUSTOM LOGO
 */
function change_logo_class( $html ) {
	// $html = str_replace('class="custom-logo"', 'your-custom-class', $html);
	$html = str_replace( 'class="custom-logo-link"', 'class="custom-logo-link"', $html );
	return $html;
}
add_filter( 'get_custom_logo', 'change_logo_class' );

function _remove_script_version( $src ) {
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

/**
 * REMOVER EMOJI
 */
function disable_wp_emojicons() {

	// all actions related to emojis
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	// filter to remove TinyMCE emojis
	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * ENABLE Shortcodes
 */
add_filter( 'comment_text', 'do_shortcode' );

add_filter( 'the_excerpt', 'do_shortcode' );

add_filter( 'widget_text', 'do_shortcode' );


/**
 * TEMP FIX FOR 4.7.1
 * Issue should be fixed in 4.7.2 in which case this will be deleted.
 */
function svgs_disable_real_mime_check( $data, $file, $filename, $mimes ) {
	$wp_filetype = wp_check_filetype( $filename, $mimes );

	$ext             = $wp_filetype['ext'];
	$type            = $wp_filetype['type'];
	$proper_filename = $data['proper_filename'];

	return compact( 'ext', 'type', 'proper_filename' );
}
add_filter( 'wp_check_filetype_and_ext', 'svgs_disable_real_mime_check', 10, 4 );


/**
 * This function modifies the main WordPress query to include an array of
 * post types instead of the default 'post' post type.
 *
 * @param object $query  The original query.
 * @return object $query The amended query.
 */
function cpt_in_search( $query ) {

	if ( $query->is_search ) {
		$query->set( 'post_type', array( 'post', 'page', 'products', 'portfolio' ) );
	}

	return $query;

}
add_filter( 'pre_get_posts', 'cpt_in_search' );


/**
 * Return the post excerpt, if one is set, else generate it using the
 * post content. If original text exceeds $num_of_words, the text is
 * trimmed and an ellipsis (…) is added to the end.
 *
 * @param  int|string|WP_Post $_id   Post ID or object. Default is current post.
 * @param  int                $num Number of words. Default is 55.
 * @return string                        The generated excerpt.
 */
function get_the_excerpt_byid( $_id = null, $num = 55 ) {
	global $post;
	$save_post = $post;
	$post      = get_post( $_id );
	if ( has_excerpt( $post->ID ) ) {
		setup_postdata( $post );
		$output  = wp_strip_all_tags( get_the_excerpt( $post ) );
		$excerpt = wp_trim_words( $output, $num );
		wp_reset_postdata();
		$post = $save_post;
	} else {
		$excerpt = '';
	}
	return $excerpt;
}

/**
 * Remove "Category:" from title
 */

add_filter(
	'get_the_archive_title',
	function ( $title ) {

		if ( is_category() ) {

					$title = single_cat_title( '', false );

		} elseif ( is_tag() ) {

			$title = single_tag_title( '', false );

		} elseif ( is_author() ) {

			$title = '<span class="vcard">' . get_the_author() . '</span>';

		}

		return $title;

	}
);
