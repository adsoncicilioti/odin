<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Odin
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if ( ! get_option( 'site_icon' ) ) : ?>
		<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/favicon.ico" rel="shortcut icon" />
	<?php endif; ?>

	<script type="text/javascript">
		window.onload = function() {
			document.getElementById('body').classList.add( 'loaded');

			var h = document.getElementById('navbar-main-navigation').offsetHeight;
			document.getElementById('page-header').style.paddingTop = h + 'px';
		};

	</script>

	<style media="screen">

	#content,
	#header,
	#footer,
	#sidebar {
		opacity: 0;
	}
	#header { transform: translateY(-50%); }
	#content { transform: translateY(8%); }
	#footer,
	#sidebar { transform: translateY(50%); }

	body.loaded  #header,
	body.loaded  #content,
	body.loaded  #footer,
	body.loaded  #sidebar {
		transform: none;
		opacity: 1;
	}

	</style>

	<?php wp_head(); ?>

</head>


<body id="body" <?php body_class(); ?>>

	<nav class="navigation-skiplink" role="navigation">
		<a class="navigation-skiplink-link" href="#content"><?php esc_html_e( 'Skip to content', 'odin' ); ?></a>
	</nav>

<header class="rel ease modal-blur" id="header" role="banner">

	<nav class="main-navigation ease navbar navbar-dark bg-navy-75 p-0 flex-column abs-top" id="navbar-main-navigation" role="navigation">

		<?php /* BARRINHA SUPERIOR */ ?>
		<div class="navbar-text navbar-top d-flex p-0 w-100 bg-navy">
			<div class="container d-flex justify-content-end align-items-center">
			<?php if ( ! empty( get_theme_mod( 'contact_mail' ) ) ) : ?>
				<a class="email px-2 small d-none d-lg-block text-white-50"
				href="mailto:<?php echo esc_attr( get_theme_mod( 'contact_mail' ) ); ?>">
					<svg class="icon text-primary h5 m-0"><use xlink:href="#envelope" /></svg>
					<?php echo esc_html( get_theme_mod( 'contact_mail' ) ); ?>
				</a>
			<?php endif; ?>
			<?php if ( get_theme_mod( 'contact_phone' ) ) : ?>
				<?php $phones = get_theme_mod( 'contact_phone' ); ?>
				<div class="tel px-2 small d-none d-lg-block">
					<svg class="icon text-primary h5 m-0"><use xlink:href="#call-phone" /></svg>
					<?php foreach ( $phones as $phone => $num ) : ?>
						<?php
						if ( isset( $num['phone_num'] ) ) {
							$number = $num['phone_num'] . ' ';
						}
						?>
						<?php echo esc_html( $number ); ?>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
				<?php
				get_template_part( 'components/navtop', 'socials' );
				get_template_part( 'components/call', 'sign' );
				?>
			</div>
		</div>

		<?php /* LOGO E MENU PRINCIPAL */ ?>
		<div class="container d-flex flex-row align-items-center">
			<div class="logo-flag z-1000 bg-white abs shadow">
				<?php get_template_part( 'components/header', 'branding' ); ?>
			</div>

			<div class="flex-grow-1">
				<div class="meganav d-flex justify-content-end rel">
					<?php get_template_part( 'components/navigation', 'top' ); ?>
					<?php get_template_part( 'components/call', 'search' ); ?>
				</div>
			</div>
		</div>

	</nav><!-- .navbar -->


<?php

/* Headers of Pages */
get_template_part( 'components/content', 'head' );

?>

</header>
