<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Odin
 * @since 2.2.0
 */

get_header();




// start of Content and Wrappers
get_template_part( 'components/content', 'start' ); ?>

	<div class="page-content">
		<div class="container entry-content px-0 bg-white shadow-secondary">
			<div class="py-5 txt-c">
				<p class="lead"><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'odin' ); ?></p>

				<?php get_search_form(); ?>
			</div>
		</div>
	</div><!-- .page-content -->

<?php // end of Content and Wrappers
get_template_part( 'components/content', 'end' ); ?>
