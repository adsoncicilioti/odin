<?php if ( !empty( get_theme_mod('home_slides'))) : ?>

    <?php

        $slides = get_theme_mod('home_slides');
        $counter = 1;
        ?>

        <div class="home-slideshow">

        <?php foreach( $slides as $slide => $sld ) : ?>

        <?php

            /* VARIABLES */

            $uniq = 'sld-'.$counter++;

            // Custom ID
            $uid = (!empty($sld[slide_uid])) ? ' id="' . $sld[slide_uid] . '"' : '';

            // slide
            $bg_color = (!empty($sld[slide_bg_color])) ? 'background-color:' . $sld[slide_bg_color] .';' : '';

            $bg_size = (!empty($sld[slide_bg_img_size])) ? 'background-size:' . $sld[slide_bg_img_size] . ';' : '';

            $bg_img = (!empty($sld[slide_bg_image])) ? 'background-image: url("' . wp_get_attachment_url($sld[slide_bg_image]) . '");' : '' ;

            $bg_rept = (!empty($sld[slide_bg_rept])) ? 'background-repeat:' . $sld[slide_bg_rept] . ';' : '';

            $bg_pos = (!empty($sld[slide_bg_pos])) ? 'background-position:' . $sld[slide_bg_pos] . ';' : '';



            // Title
            $tit_align = (!empty($sld[slide_tit_align])) ? ' text-' . $sld[slide_tit_align] : '';
            $tit_color = (!empty($sld[slide_title_color])) ? 'color:' . $sld[slide_title_color] .';' : '';

            $tit_styles = $sld[slide_tit_style];
            $tstyle = '';

              foreach( $tit_styles as $ts_val ) {

                $tstyle .= ' ' . $ts_val;
              }





            $tit_size = (!empty($sld[slide_tit_size])) ? ' ' . $sld[slide_tit_size] : '';


             // Description
            $desc_color = (!empty($sld[slide_desc_color])) ? 'color:' . $sld[slide_desc_color] . ';': '';

            // HEAD
            if ( !empty( $sld[slide_image] )) {
                $head_reverse = ( $sld[slide_head_col] === 'half-l' ||  $sld[slide_head_col] == 'img-r-out' ) ? ' flex-row-reverse' : '';
            } else {
               $head_reverse = ( $sld[slide_head_col] === 'half-r' ||  $sld[slide_head_col] == 'img-l-out' ) ? ' flex-row-reverse' : '';
            }
            $head_col = ( !empty($sld[slide_head_col]) ) ? ' col-md-6' : ' col-12';
            $img_align = $head_col;
            $imgout_class = ' mx-auto';

            // IMG Align
            if ( !empty( $sld[slide_head_col] ))  {

                if ($sld[slide_head_col] == 'half-r' ) {
                    $img_align .= ' pr-md-4';
                } elseif ($sld[slide_head_col] == 'half-l') {
                    $img_align .= ' pl-md-4';
                }

                if ( $sld[slide_head_col] == 'img-l-out') {
                    $imgout_class = ' float-md-right';

                } elseif (  $sld[slide_head_col] == 'img-r-out' ) {
                    $imgout_class = ' float-md-left';
                }


                if ( in_array($sld[slide_head_col],array('img-l-out', 'img-r-out' ) )) {
                    $mw180 = ' mw-180';
                }


            } else {
                $img_align .= ' mb-3';
            }

            // IMG fx


            $img_style = ( !empty($sld[slide_img_style])) ? ' ' . $sld[slide_img_style] : '';

            // Button
            $btn_txt = ( !empty( $sld[slide_btntxt] ) ) ?  $sld[slide_btntxt] : '' ;
            $btn_link = ( !empty( $sld[slide_btnuri] ) ) ?  $sld[slide_btnuri] : '' ;

            if ( !empty($sld[slide_btncolor]) ) {
                $btn_color = ' btn-' . $sld[slide_btncolor];
                if ( !empty($sld[slide_btnout])) {
                   $btn_color = ' btn-outline-' . $sld[slide_btncolor];
                }
            } else {
                $btn_color = '';
            }

        ?>

            <article <?php echo $uid; ?> class="home-slides rel d-flex align-items-center <?php echo $uniq; ?>">

            <?php if (
                !empty( $bg_color ) ||
                !empty( $tit_color ) ||
                !empty( $desc_color ) ||
                !empty( $bg_img )
            )  : ?>

            <?php echo '<style>'; ?>

                <?php  /*slide Css*/ if (
                    !empty($bg_color) ||
                    !empty( $bg_img )
                ) : ?>
                <?php echo '.' . $uniq; ?> {
                    <?php echo $bg_color . $bg_img . $bg_fix . $bg_size . $bg_rept . $bg_pos; ?>
                }
                <?php endif; ?>

                <?php /* Title CSS */ if (!empty($tit_color)) : ?>
                <?php echo '.' . $uniq . ' .sld-title'; ?> {
                    <?php echo $tit_color; ?>
                }
                <?php endif; ?>

                <?php /* Description CSS */  if (!empty($desc_color)) : ?>
                <?php echo '.' . $uniq . ' .sld-desc'; ?> {
                    <?php echo $desc_color; ?>;
                }
                <?php endif; ?>

            <?php echo '</style>'; ?>
            <?php endif; // For Style tag ?>

            <div class="cf container py-5">
                <div class="row align-items-center<?php echo $head_reverse; ?>">

                <?php if ( !empty( $sld[slide_image] ) ) : ?>
                <div class="my-4 sld-img-wrap<?php echo $img_align; ?>">
                    <?php echo wp_get_attachment_image( $sld[slide_image], 'full', false, array( "class" => "sld-img img-fluid" . $imgout_class . $img_style . $mw180 ) ); ?>
                </div>
                <?php endif;?>

                <?php if (!empty($sld[slide_title]) || !empty($sld[slide_desc])) : ?>
                <div class="<?php echo $tit_align . $head_col; ?>">

                    <?php if (!empty($sld[slide_title])) : ?>
                        <h2 class="sld-title mb-4<?php echo $tstyle  . $tit_size;?>"><?php echo $sld[slide_title]; ?></h2>
                    <?php endif;?>

                    <?php if (!empty($sld[slide_desc]) || !empty( $btn_txt ) ) : ?>

                        <div class="lead sld-desc"><?php echo do_shortcode( $sld[slide_desc] );?></div>

                        <?php if ( !empty( $btn_txt ) ) : ?>
                            <p class="mt-3 txt-up"><a href="<?php echo $btn_link; ?>" class="btn px-md-4 px-3<?php echo $btn_color . $btn_style;?>"><?php echo $btn_txt; ?></a></p>
                        <?php endif; ?>

                    <?php endif;?>
                </div>
                <?php endif;?>


                </div>
            </div>

        </article>
    <?php endforeach; ?>

</div>
<?php endif; ?>
