<?php
/**
 * The template for displaying a "No posts found" message.
 *
 * @package Odin
 * @since 2.2.0
 */
?>


<div class="page-content no-radius card p-3 bg-white  hentry">
	<header class="page-header cf z1 rel pt-3 px-3 bg-dark2">
	<div class="container">
		<h1 class="h2 font-light page-title"><span class="ico-alert float-xs-left pr-1"></span> <?php _e( 'Nothing Found', 'odin' ); ?></h1>
	</div>
	</header>
	<div class="container">
	
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

		<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'odin' ), admin_url( 'post-new.php' ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

		<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'odin' ); ?></p>
		<?php get_search_form(); ?>

	<?php else : ?>

		<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'odin' ); ?></p>
		<?php get_search_form(); ?>

	<?php endif; ?>
	
	</div>
</div><!-- .page-content -->
