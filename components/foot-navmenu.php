<?php if ( get_theme_mod("foot_menunav") == "yes" ) : ?>
				<div class="col-lg-4 mt-2 txt-c">
					<h3 class="h4 mb-2 txt-up">Continue Navegando</h3>
					<nav cass="cf" role="navigation">
						<?php
						wp_nav_menu(
						array(
						'theme_location' => 'foot-menu',
						'depth'          => 1,
						'container'      => false,
						'menu_class'     => 'menu-foot'
						)
						);
						?>

					</nav><!-- .navbar -->
				</div>
				<?php endif; ?>