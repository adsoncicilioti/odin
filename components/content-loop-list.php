<?php
/**
 * The default template for displaying content.
 *
 * Used for loop in index/archive/author/catagory/search/tag.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
<div class="col-12 mb-4">

<article id="post-<?php the_ID(); ?>" <?php post_class( 'rounded-0 card bg-white ' ); ?>>
	<div class="row ">
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="col-lg-5 col-md-6 pr-md-0">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php echo odin_thumbnail( 540, 360, get_the_title(), $crop = true, 'card-img-top rounded-0 w-100' ); ?>
			</a>
		</div>
	<?php endif; ?>
	<div class="col d-flex flex-column justify-content-between pl-md-0">
		<div class="card-block p-3">
			<header class="entry-header">
				<?php
					the_title( '<h2 class="h5 card-title entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				?>
				<?php if ( 'post' == get_post_type() ) : ?>
					<div class="entry-meta mb-2 text-muted small">
						<?php odin_posted_on(); ?>
					</div><!-- .entry-meta -->
				<?php endif; ?>
			</header><!-- .entry-header -->

			<div class="entry-summary">
				<?php echo '<p class="card-text text-justify small">' . odin_excerpt( 'excerpt', 15 ) . '</p>'; ?>
			</div><!-- .entry-summary -->

		</div>

		<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) ) : ?>
			<footer class="rounded-0 card-footer entry-meta p-y-2 px-3">
				<span class="cat-links pr-1">
					<i class="ico-folder"></i>
					<?php echo __( 'Posted in:', 'odin' ) . ' ' . get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'odin' ) ); ?>
				</span>
				<?php the_tags( '<span class="tag-links pr-1"><i class="ico-tag"></i> ' . __( 'Tagged as:', 'odin' ) . ' ', ', ', '</span>' ); ?>
				<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
					<span class="comments-link">
						<i class="ico-msg"></i>
						<?php comments_popup_link( __( 'Comment', 'odin' ), __( '1 Comment', 'odin' ), __( '% Comments', 'odin' ) ); ?>
					</span>
				<?php endif; ?>
			</footer>
		<?php endif; ?>
	</div>
	</div>
</article><!-- #post-## -->
</div>
