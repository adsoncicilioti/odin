<?php
/**
 * Nav Sign
 *
 * Main user sign way for this theme.
 *
 * @package WordPress
 * @subpackage Odin
 * @version 1.0.0
 * @author  Adson Cicilioti
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 */

global $user_ID, $user_identity;
wp_get_current_user();

/* If not Logged  in */
if ( ! $user_ID ) :
	$register = '';
	$reset    = '';

	if ( isset( $_GET['register'] ) && wp_verify_nonce( sanitize_key( $_GET['register'] ) ) ) {
		$register = sanitize_text_field( wp_unslash( $_GET['register'] ) );
	}

	if ( isset( $_GET['reset'] ) && wp_verify_nonce( sanitize_key( $_GET['reset'] ) ) ) {
		$reset = sanitize_text_field( wp_unslash( $_GET['reset'] ) );
	}

	if ( true === $register ) :
		?>

<div class="logreg-alert fixed force-center alert alert-success bg-success alert-dismissible fade in" role="alert">
	<span class="close abs rounded-lg btn-danger" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</span>
	<h3 class="alert-heading"><?php esc_html_e( 'Success!', 'odin' ); ?></h3>
	<p><?php esc_html_e( 'A password has been sent to your email.', 'odin' ); ?></p>
</div>

		<?php elseif ( true === $reset ) : ?>

<div class="logreg-alert fixed force-center alert alert-success bg-success alert-dismissible fade in" role="alert">
	<span class="close abs rounded-lg btn-danger" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</span>
	<h3 class="alert-heading"><span class="float-xs-left pr-1">&checkmark;</span> Sucesso!</h3>
	<p>Enviamos instruções para redefinir sua senha para o seu email.</p>
</div>

		<?php endif; ?>

<div class="modal fade ease" id="signPup"  tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content text-body">

			<input type="radio" class="sr-only" name="itabs" id="itab_login" checked="checked">
			<input type="radio" class="sr-only" name="itabs" id="itab_register">
			<input type="radio" class="sr-only" name="itabs" id="itab_resetpass">

			<div class="modal-header bg-gray-200 pb-0 border-0">
				<ul class="nav nav-tabs w-100 border-0 rounded-0 text-black-50 txt-bold">
					<li class="nav-item m-0">
						<label for="itab_login" class="nav-link _login m-0">
							<?php esc_html_e( 'Login', 'odin' ); ?>
						</label>
					</li>
					<li class="nav-item m-0">
						<label for="itab_register" class="nav-link _register m-0 rounded-0">
							<?php esc_html_e( 'Signup', 'odin' ); ?>
						</label>
					</li>
				</ul>

				<button type="button" class="close no-bg" data-dismiss="modal" aria-label="Close">
					<span class="over-close rel d-block"></span>
				</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<div id="tab_login" class="itab_content ease">
					<p class="lead text-primary txt-c"><?php esc_html_e( 'Enter in your account', 'odin' ); ?></p>
					<form method="post" action="<?php site_url(); ?>/wp-login.php" class="wp-user-form">
						<div class="form-group username">
							<label class="col-form-label-lg p-0" for="user_login"><?php esc_html_e( 'Username', 'odin' ); ?>: </label>
							<input class="form-control form-control-lg" type="text" name="log" id="user_login" tabindex="11" required="required"/>
						</div>
						<div class="form-group password">
							<label class="col-form-label-lg p-0" for="user_pass"><?php esc_html_e( 'Password', 'odin' ); ?>: </label>
							<input class="form-control form-control-lg" type="password" name="pwd" id="user_pass" tabindex="12" required="required"/>
						</div>
						<div class="form-group txt-r mb-0">

							<div class="custom-control custom-checkbox rememberme mb-3 txt-l">
								<input type="checkbox" class="custom-control-input" name="rememberme" value="forever" checked="checked" id="rememberme" tabindex="13">
								<label class="custom-control-label" for="rememberme"> <?php esc_html_e( 'Remember me', 'odin' ); ?></label>
							</div>

							<?php do_action( 'login_form' ); ?>
							<button class="btn btn-block btn-lg btn-primary txt-up txt-bold" type="submit" name="user-submit"  tabindex="14">
								<?php esc_html_e( 'Enter', 'odin' ); ?>
							</button>
							<input type="hidden" name="redirect_to" value="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>" />
							<input type="hidden" name="user-cookie" value="1" />

						</div>
						<label for="itab_resetpass" class="btn btn-block btn-link _resetpass mt-2 mb-0 ">
							<?php esc_html_e( 'Forgot your password?', 'odin' ); ?>
						</label>
					</form>
				</div>

				<div id="tab_register" class="itab_content ease ">
					<p class="lead text-primary txt-c"><?php esc_html_e( 'Register and follow our updates!', 'odin' ); ?></p>
					<form method="post" action="<?php echo esc_url( site_url( 'wp-login.php?action=register', 'login_post' ) ); ?>" class="wp-user-form">
						<div class="form-group username">
							<label class="col-form-label-lg p-0" for="r_user_login"><?php esc_html_e( 'Username', 'odin' ); ?>: </label>
							<input class="form-control form-control-lg" type="text" name="user_login" id="r_user_login" tabindex="101" required="required"/>
						</div>
						<div class="form-group password">
							<label class="col-form-label-lg p-0" for="user_email"><?php esc_html_e( 'E-mail', 'odin' ); ?>: </label>
							<input class="form-control form-control-lg" type="email" name="user_email" id="user_email" tabindex="102" required="required" />
						</div>
						<div class="form-group txt-r mb-0">
							<?php do_action( 'register_form' ); ?>
							<button class="btn btn-lg btn-block btn-primary txt-up txt-bold" type="submit" name="user-submit"  tabindex="103">
								<?php esc_html_e( 'Sign me up!', 'odin' ); ?>
							</button>
							<?php

							if ( true === $register ) {
								echo '<p>Uma senha de acesso foi enviada para o seu email!</p>';
							}
							?>
							<input type="hidden" name="redirect_to" value="<?php echo esc_url( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>?register=true" />
							<input type="hidden" name="user-cookie" value="1" />
						</div>
						<label for="itab_login" class="btn btn-block btn-link _login mt-2 mb-0 ">
							<?php esc_html_e( 'Already have an account?', 'odin' ); ?>
						</label>
					</form>
				</div>

				<div id="tab_resetpass" class="itab_content ease">
					<p class="lead text-primary txt-c"><?php esc_html_e( 'Enter your email or username to recover your password.', 'odin' ); ?></p>
					<form method="post" action="<?php echo esc_url( site_url( 'wp-login.php?action=lostpassword', 'login_post' ) ); ?>" class="wp-user-form">
						<div class="form-group username">
							<label class="col-form-label-lg p-0" for="p_user_login" class="hide"><?php esc_html_e( 'Username or Email', 'odin' ); ?>: </label>
							<input class="form-control form-control-lg" type="text" name="user_login" value="" id="p_user_login" tabindex="1001" required="required"/>
						</div>
						<div class="form-group mb-0">
							<?php do_action( 'login_form', 'resetpass' ); ?>
							<button class="btn btn-block btn-lg btn-primary txt-up txt-bold" type="submit" name="user-submit"  tabindex="1002">
								<?php esc_html_e( 'New password!', 'odin' ); ?>
							</button>
							<?php

							if ( true === $reset ) {
								esc_html_e( '<p>We sent instructions went to your email.</p>', 'odin' );
							}
							?>
							<input type="hidden" name="redirect_to" value="<?php echo esc_url( wp_unslash( $_SERVER['REQUEST_URI'] ) ); ?>?reset=true" />
							<input type="hidden" name="user-cookie" value="1" />
						</div>
					</form>
				</div>
			</div> <!-- End Modal body -->
		</div>
	</div>
</div><!-- End #signPup (not Logged) -->

<?php else : ?>

<div class="modal fade px-3 ease" id="signPup" tabindex="-1" role="dialog" aria-labelledby="signPupTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content text-body">
			<div class="modal-header bg-primary text-white pb-2">
				<h3 class="modal-title" id="signPupTitle"><?php esc_html_e( 'Hello!', 'odin' ); ?></h3>
				<button type="button" class="close no-bg" data-dismiss="modal" aria-label="Close">
					<span class="over-close rel d-block"></span>
				</button>
			</div>
			<div class="modal-body pt-3">
				<p class="lead"><?php esc_html_e( 'You are logged in as ', 'odin' ); ?>
					<strong><?php echo esc_html( $user_identity ); ?></strong>
				</p>
				<nav class="btn-group-vertical rounded-top-0 w-100" >

		<?php if ( current_user_can( 'manage_options' ) ) : ?>

					<a class="btn btn-lg btn-link text-primary" href="<?php echo esc_url( get_edit_post_link() ); ?>"> <?php esc_html_e( 'Edit this Page', 'odin' ); ?></a>
					<a class="btn btn-lg btn-link text-primary" href="<?php echo esc_url( admin_url( 'customize.php' ) ); ?>"> <?php esc_html_e( 'Customize this Page', 'odin' ); ?></a>
					<a class="btn btn-lg btn-link text-primary" href="<?php echo esc_url( admin_url() ); ?>"><?php esc_html_e( 'Admin Panel', 'odin' ); ?></a>

		<?php endif; ?>

					<a class="btn btn-lg btn-link text-primary" href="<?php echo esc_url( admin_url( 'profile.php' ) ); ?>"> <?php esc_html_e( 'Edit profile', 'odin' ); ?></a>
					<a class="btn btn-lg btn-light text-danger" href="<?php echo esc_url( wp_logout_url( 'index.php' ) ); ?>"><?php esc_html_e( 'Logout', 'odin' ); ?></a>
				</nav>
			</div>
		</div>
	</div>
</div><!-- End #itab-login-register -->
<?php endif; ?>
