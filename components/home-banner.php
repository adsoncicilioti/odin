<?php
/**
 * Home Banner component
 * ACF plugins is required
 */

$hb_img    = get_field( 'home_banner_img' );
$hb_align  = get_field( 'home_banner_content_align' );
$hb_pos    = get_field( 'home_banner_content_pos' );
$hb_enable = get_field( 'home_banner_enable' );

if ( function_exists( 'pll_current_language' ) ) {
	$lang = ( 'en' === pll_current_language() ) ? '_en' : '';
}
$btn_field   = 'home_banner_button' . $lang;
$title_field = 'home_banner_title' . $lang;


$hb_btn = get_field( $btn_field ) ? get_field( $btn_field ) : '';

$hb_target  = ( ! empty( $hb_btn['target'] ) ) ? ' target=' . $hb_btn['target'] : '';
$hb_btn_txt = ( isset( $hb_btn['title'] ) ) ? $hb_btn['title'] : '';
$hb_btn_url = ( isset( $hb_btn['url'] ) ) ? $hb_btn['url'] : '';
?>


<section id="page-header" class="home_banner d-flex">
	<?php if ( $hb_enable ) : ?>
	<div class="container d-flex align-items-center justify-content-<?php echo sanitize_html_class( $hb_pos ); ?>">

		<div class="banner-content p-4 bg-black-50 text-<?php echo sanitize_html_class( $hb_align ); ?>">
			<h1 class="banner-title text-white txt-bold">
				<?php the_field( $title_field ); ?>
			</h1>
		<?php if ( ! empty( $hb_btn ) ) : ?>
			<p class="m-0">
				<a
					href="<?php echo esc_url_raw( $hb_btn_url ); ?>"
					<?php echo esc_attr( $hb_target ); ?>
					class="banner-btn btn btn-primary txt-up">
					<?php echo esc_html( $hb_btn_txt ); ?>
				</a>
			</p>
		<?php endif; ?>
		</div>
	</div>
	<style>
		.home_banner {
			<?php if ( $hb_img ) : ?>
			background-image: url('<?php echo esc_url_raw( $hb_img ); ?>');
			background-position: center;
			background-size: cover;
			background-attachment: fixed;
			<?php endif ?>
			padding-bottom: 5rem;
			margin-bottom: -5rem;
			min-height: 600px;
			height: 100vh;
		}
		.home_banner .banner-btn {
			margin-bottom: -3rem;
		}
		.home_banner .banner-content p {
			line-height: 0;
		}
	</style>

<?php elseif ( current_user_can( 'manage_options' ) ) : ?>
	<?php
		echo '<div class="container d-flex align-items-center justify-content-center"><div class="txt-c p-4"><h3>' , esc_html( 'Setup a Homepage Banner to appear here', 'odin' ) , '</h3>
		<p><a class="btn mt-4 btn-lg btn-outline-dark" href="' , esc_url( get_edit_post_link() ) , '">' , esc_html_e( 'Edit this Page', 'odin' ) , '</a></p></div></div>'
	?>
<?php else : ?>
	<div class="container d-flex align-items-center justify-content-center">
		<div class="banner-content p-4 bg-black-50 text-center">
			<h1 class="banner-title text-white txt-bold">
				<?php echo esc_html( get_bloginfo( 'description' ) ); ?>
			</h1>
		</div>
	</div>
<?php endif; ?>
</section>
