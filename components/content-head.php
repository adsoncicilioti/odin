<?php
	global $post;

	$has_img     = has_post_thumbnail() ? has_post_thumbnail( $post->ID ) : '';
	$bgclass     = ( is_404() ) ? 'bg-danger txt-c' : 'bg-primary';
	$bgclass     = ( $has_img ) ? 'bg-navy' : $bgclass;
	$image       = ( $has_img ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0] : '';
	$head_height = ( $has_img ) ? ' vh-75-min' : ' vh-50-min';

if ( is_front_page() && is_page_template('Home-Page') ) :
	get_template_part( 'components/home', 'banner' );
else :
	?>

<section id="page-header" class="page-header z-1 rel pb-5 d-flex align-items-center <?php echo esc_attr( $bgclass ) . ' ' . esc_attr( $head_height ); ?>">


	<?php if ( $has_img ) : ?>
	<style type="text/css">
		#page-header:before {
			content: '';
			position: absolute;
			top: 0;
			bottom: 0;
			width: 100%;
			opacity: .3;
			z-index: -1;
			background-image: url('<?php echo esc_url( $image ); ?>');
			background-position: center;
			background-size: cover;
			background-attachment: fixed;
		}
	</style>
	<?php endif; ?>

	<header class="container text-white txt-c">

		<h1 class="page-title display-4">
		<?php

		// Setup title of list pages

		if ( is_search() ) :
			printf( __( 'Search Results for: %s', 'odin' ), get_search_query() );
		elseif ( is_archive() ) :
			the_archive_title();
		elseif ( is_404() ) :
				echo '<svg class="icon mr-2"><use xlink:href="#triangle-danger"></use></svg>';
				esc_html_e( 'Error 404! Page not found', 'odin' );
		else :
			if ( is_home() ) {
				wp_title( '' );
			} else {
				the_title();
			}
		endif;
		?>
		</h1>

		<?php
		// EXCERPT && Descriptions CONDITIONALS
		/* List pages */

		if ( has_excerpt() && ! is_home() && ! is_search() ) :
			?>
			<p class="lead"><?php echo esc_html( odin_excerpt( 'excerpt', 30 ) ); ?></p>
		<?php elseif ( is_home() ) : ?>
			<p class="lead"><?php esc_html_e( 'Check out all the publications and news from our site.', 'odin' ); ?></p>
		<?php elseif ( is_search() ) : ?>
			<p class="lead"><?php printf( __( 'Check out all the results of your search by: %s', 'odin' ), get_search_query() ); ?></p>
		<?php elseif ( is_archive() ) : ?>
			<p class="lead"><?php the_archive_description(); ?></p>
		<?php elseif ( is_author() ) : ?>
			<?php if ( get_the_author_meta( 'description' ) ) : ?>
				<div class="author-bio p-y-1">
					<span class="author-avatar">
						<?php echo get_avatar( get_the_author_meta( 'ID' ), 60, null, null, array( 'class' => array( 'img-rounded' ) ) ); ?>
					</span>
					<p class="lead"><?php the_author_meta( 'description' ); ?></p>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</header>
</section><!-- .page-header -->
<?php endif; ?>

<?php

// BREADCRUMB

if ( ! is_search() && ! is_home() && ! is_front_page() && ! is_archive() ) {
	if ( function_exists( 'odin_breadcrumbs' ) ) {
		echo '<div class="bread-wrap rel container p-0 -mt-5">';
		odin_breadcrumbs();
		echo '</div>';
	}
}
?>
