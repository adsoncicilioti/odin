<?php
// Sidebar pages
if ( is_home() || is_archive() || is_search() || is_page_template( array( 'theme-blog.php', 'theme-sidebar.php' ) ) ) : ?>

<div class="container">
	<div class="row">
		<main id="content" class="<?php echo sanitize_html_class( odin_classes_page_sidebar() ); ?> -mt-5  modal-blur" role="main" tabindex="-1">
		<div class="row">
	<?php
else :
	// Fullwidth pages
	?>

	<main id="content" class="<?php echo sanitize_html_class( odin_classes_page_full() ); ?> p-0 pb-4 over-hide  modal-blur" role="main" tabindex="-1">

<?php endif; ?>
