<?php
/**
 * The default template for displaying content.
 *
 * Used for single and (in future) post formats.
 *
 * @package Odin
 * @since 2.2.0
 */

// Loop pages
if ( is_home() || is_archive() || is_search() || is_page_template( array( 'theme-blog.php' ) ) ) :

	get_template_part( 'components/content', 'loop-list' );


	// elseif (condition) :

else :
	get_template_part( 'components/content', 'page' );

endif;
