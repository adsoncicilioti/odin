<?php	odin_the_custom_logo(); ?>

<div>
	<?php if ( is_front_page() || is_home() ) : ?>
		<h1 class="site-title m-0">
			<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
		</h1>
	<?php else : ?>
		<div class="site-title m-0">
			<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
		</div>
	<?php endif; ?>

	<?php if ( get_bloginfo( 'description' ) || is_customize_preview() ) : ?>
		<p class="site-description m-0"><small><?php bloginfo( 'description' ); ?></small></p>
	<?php endif ?>

</div>
