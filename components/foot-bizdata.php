


<?php if( get_theme_mod('street_address')) :?>
	<div class="vcard small">
		<address class="my-0" itemscope itemtype="http://schema.org/ContactPoint" >
			<div class="d-flex justify-content-center justify-content-lg-start" itemscope itemtype="schema.org/PostalAddress" >

				<i class="d-none d-lg-inline-block"><svg class="icon">
				<use xlink:href="#pin-map" />
				</svg></i>

				<div class="pl-1">
					<span itemprop="streetAddress" class="street-address">
						<?php echo get_theme_mod('street_address'); ?>,
					</span>

					<?php if( get_theme_mod('city_state') ) : ?>
						<span itemprop="addressLocality" class="locality">
						<?php echo get_theme_mod('city_state'); ?></span>,
					<?php endif; ?>

					<?php if( get_theme_mod('cep') ) : ?>
						CEP <span itemprop="postalCode" class="postal-code"><?php echo get_theme_mod('cep'); ?></span>,
					<?php endif; ?>

					<?php if( get_theme_mod('country') ) : ?>
						<span itemprop="addressCountry" class="country-name">
							<?php echo get_theme_mod('country'); ?>
						</span>
					<?php endif; ?>

				</div>
			</div>
		</address>

		<?php if( get_theme_mod('contact_mail') ) : ?>
			<div class="pt-2 d-flex justify-content-center justify-content-lg-start" >
				<i class="d-none d-lg-inline-block"><svg class="icon">
				<use xlink:href="#envelope" />
				</svg></i>
				<div class="pl-1">
					<?php echo get_theme_mod('contact_mail'); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if ( !empty( get_theme_mod('contact_phone') ) ) : ?>
			<div class="pt-2 d-flex justify-content-center justify-content-lg-start" >
				<i class="d-none d-lg-inline-block">
					<svg class="icon"><use xlink:href="#call-phone" /></svg>
				</i>
				<div class="pl-1">
					<?php $phones = get_theme_mod('contact_phone'); ?>
					<?php foreach( $phones as $phone => $num ) : ?>
						<?php
							if (isset($num[phone_num])) {
								$number = $num[phone_num] . '<br>';
							}
						?>
						<?php echo $number; ?>
				<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>


	</div>
<?php else : ?>
	<a class="btn btn-outline-secondary" href="<?php home_url(); ?>/wp-admin/customize.php"><?php _e('Insert your business data', 'odin'); ?> </a>
<?php endif; ?>

