<?php
/**
 * SOCIAL LINKS
 */

$socials = get_theme_mod( 'social_links' );

if ( $socials ) : ?>
	<div class="text-lg-right">
			<?php
			foreach ( $socials as $social => $attrs ) {

				switch ( $attrs['social_type'] ) {

					case 'facebook':
					case 'twitter':
					case 'pinterest':
					case 'youtube':
					case 'instagram':
					case 'vimeo':
						$base_url = 'https://' . $attrs['social_type'] . '.com/';
						break;
					case 'google-plus':
						$base_url = is_numeric( $attrs['social_user'] ) ? 'https://plus.google.com/' : 'https://plus.google.com/+';
						break;
					case 'linkedin':
						$base_url = ( strpos( $$attrs['social_user'], 'linkedin.com/company' ) !== false ) ? 'https://' . $attrs['social_type'] . '.com/company/' : 'https://' . $attrs['social_type'] . '.com/in/';
						break;
					case 'whatsapp':
						$base_url = 'https://api.whatsapp.com/send?text=' . __( 'Hi!', 'odin' ) . '&phone=';
						break;
				}

				$full_url = $base_url . substr( strrchr( '/' . $attrs['social_user'], '/' ), 1 ); // Hack Elias

				echo '<a href="' . esc_attr( $full_url ) . '" target="_blank" title="' . __( 'Go to our ', 'odin' ) . esc_attr( ucfirst( $attrs['social_type'] ) ) . '"><svg class="icon"><use xlink:href="#' . esc_attr( $attrs['social_type'] ) . '"></use></svg><span class="sr-only">' . esc_html( ucfirst( $attrs['social_type'] ) ) . '</span></a>';
			}
			?>

		</div>
	</div>

<?php endif; ?>
