<?php
// Sidebar pages
if ( is_home() || is_archive() || is_search() || is_page_template( array( 'theme-blog.php', 'theme-sidebar.php' ) ) ) : ?>
			</div> <!-- end row of articles-->
		</main>
		<?php get_sidebar(); ?>
	</div>
</div>
	<?php get_footer(); ?>

	<?php // elseif (condition) : ?>

<?php else :
	// Fulwidth Pages

	echo '</main>';
	get_footer();
endif; ?>
