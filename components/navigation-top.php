		<input class="sr-only" type="checkbox" name="btn-toggle" id="toggle-nav">

		<?php

			$home_icon = '<svg class="icon d-none d-lg-inline"><use xlink:href="#home-f" /></svg>'; // Return not echo
			$home_url  = site_url();
			$is_active = ''; // true
		if ( is_front_page() ) {
			$is_active = 'current-menu-item';
		}

			$items_wrap  = '<ul id="%1$s" class="%2$s">%3$s';
			$items_wrap .= sprintf( '<li class="menu-item home-menu-item %3$s"><a href="%2$s" class="nav-link">%1$s<span class="d-lg-none">Home</span></a></li></ul>', $home_icon, $home_url, $is_active );

			wp_nav_menu(
				array(
					'items_wrap'     => $items_wrap,
					'theme_location' => 'main-menu',
					'depth'          => 3,
					'container'      => false,
					'menu_class'     => 'menu-top ease nav navbar-nav',
					'walker'         => new walkernav(),
				// 'fallback_cb'    => 'Odin_Bootstrap_Nav_Walker::fallback',
				// 'walker'      => new Odin_Bootstrap_Nav_Walker(),
				)
			);
			?>

		<label class="btn-toggle navbar-toggle d-lg-none float-xs-right" for="toggle-nav">
			<span class="sr-only"><?php _e( 'Menu', 'odin' ); ?></span>
			<div class="ico-group">
				<div class="ham-bar"></div>
				<div class="ham-bar"></div>
				<div class="ham-bar"></div>
			</div>
		</label>
