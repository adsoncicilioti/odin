<?php if ( !empty( get_theme_mod('home_blocks') ) ) : ?>

    <?php
        $blocks = get_theme_mod('home_blocks');
        $counter = 1;

        foreach( $blocks as $block => $hbk ) : ?>

        <?php

            /* VARIABLES */

            $uniq = 'hbk-'.$counter++;

            // Custom ID
            $uid = (!empty($hbk[block_uid])) ? ' id="' . $hbk[block_uid] . '"' : '';

            // Block
            $bg_color = (!empty($hbk[block_bg_color])) ? 'background-color:' . $hbk[block_bg_color] .';' : '';

            $bg_fill = (!empty($hbk[block_bg_img_fill])) ? 'background-size:' . $hbk[block_bg_img_fill] . ';' : '';

            $bg_fix = (!empty($hbk[block_bg_fix])) ? 'background-attachment: fixed;' : '';

            $bg_img = (!empty($hbk[block_bg_image])) ? 'background-image: url("' . wp_get_attachment_url($hbk[block_bg_image]) . '");' : '' ;

            $bg_rept = (!empty($hbk[block_bg_rept])) ? 'background-repeat:' . $hbk[block_bg_rept] . ';' : '';

            $bg_pos = (!empty($hbk[block_bg_pos])) ? 'background-position:' . $hbk[block_bg_pos] . ';' : '';

            // Divider
            if ( $hbk[block_divtop] == 'leaves' ) {
                $div_top = '<svg class="abs div-top div-leaves" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="90px" viewBox="0 0 1200 90" preserveAspectRatio="none"><style type="text/css">.st0{opacity:0.5;;}</style><polygon class="st0" points="874.6,51.6 882.5,63 899.8,38.9 1024.5,20 1008.7,64.2 1058,13.6 1166.6,11.9 1200,23.1 1200,90
                	618.4,90 619.3,89.9 "/><polygon points="942.6,90 436.7,38.8 436.3,61.3 393.7,24.9 157.8,0 201.7,55.3 140.9,8.7 0,24.5 0,90 "/></svg>';
            } elseif ( $hbk[block_divtop] == 'convex' ) {
                $div_top = '<svg class="abs div-top div-convex" version="1.1" xmlns="http://www.w3.org/2000/svg" width="100%" height="90px" viewBox="0 0 1200 90" preserveAspectRatio="none"><path d="M599.9,67.6C392.6,67.6,191.5,44.1,0,0v90h1200V0h-0.1C1008.3,44.1,807.2,67.6,599.9,67.6z"/></svg>';
            }elseif ( $hbk[block_divtop] == 'climb' ) {
                $div_top = '<svg class="abs div-top div-climb" version="1.1" xmlns="http://www.w3.org/2000/svg" width="100%" height="90px" viewBox="0 0 1200 90" preserveAspectRatio="none"><polygon points="1200,0 0,90 1200,90 "/></svg>';
            } else {
                $div_top = '';
            }

            $zid = ( in_array($hbk[block_divtop], array('climb', 'leaves', 'convex')) ) ? ' z1' : '';

            $space = ( !empty($hbk[block_compdiv]) ) ? '<div class="w-100 div-comp"></div>' : '';

            // Animation

            if ( $hbk[block_anim] == 'h' ) {
                $anims = array(' sr-r', ' sr-l');

            } elseif ( $hbk[block_anim] == 'v' ) {
                $anims = array(' sr-t', ' sr-b');

            } elseif ( $hbk[block_anim] == 'f' ) {
                $anims = array(' sr-fade', ' sr-fade');

            } else {
                $anims = '';
            }


            // Title
            $tit_align = (!empty($hbk[block_tit_align])) ? ' text-' . $hbk[block_tit_align] : '';

            $tit_color = (!empty($hbk[block_title_color])) ? 'color:' . $hbk[block_title_color] .';' : '';

             // Description
            $desc_align = (!empty($hbk[block_desc_align])) ? ' text-' . $hbk[block_desc_align] : '';
            $desc_color = (!empty($hbk[block_desc_color])) ? 'color:' . $hbk[block_desc_color] . ';': '';

            // IMG Align
            if ( $hbk[block_img_align] != '' )  {

                if ($hbk[block_img_align] == 'left' ) {
                    $img_align = ' pr-md-4 float-md-left';
                } elseif ($hbk[block_img_align] == 'right') {
                    $img_align = ' pl-md-4 float-md-right';
                }

                if ( $hbk[block_img_align] == 'out-left') {
                    $img_align = ' col-md-6';
                    $imgout_class = ' float-md-right';

                } elseif (  $hbk[block_img_align] == 'out-right' ) {
                    $img_align = ' col-md-6';
                    $imgout_class = ' float-md-left';
                }

                if ( in_array($hbk[block_img_align],array('out-left', 'out-right' ) )) {
                    $mw180 = ' mw-180';
                }

                if ($hbk[block_img_align] == 'center') {
                    $img_align = ' mx-auto text-center';
                }

            } else {
                $img_align = '';
            }

            // IMG Shadow

            $img_shadow = ( !empty($hbk[block_img_shad])) ? 'box-shadow: 0 1rem 2.5rem ' . $hbk[block_img_shad] . ';' : '';

            $img_style = ( !empty($hbk[block_img_style])) ? ' ' . $hbk[block_img_style] : '';

            // Button
            $btn_txt = ( !empty( $hbk[block_btntxt] ) ) ?  $hbk[block_btntxt] : '' ;
            $btn_link = ( !empty( $hbk[block_btnuri] ) ) ?  $hbk[block_btnuri] : '' ;

            if ( !empty($hbk[block_btncolor]) ) {
                $btn_color = ' btn-' . $hbk[block_btncolor];
                if ( !empty($hbk[block_btnout])) {
                   $btn_color = ' btn-outline-' . $hbk[block_btncolor];
                }
            } else {
                $btn_color = '';
            }

        ?>

        <section <?php echo $uid; ?> class="home-blocks rel py-5 cf <?php echo $uniq . $zid; ?>">

            <?php if (
                !empty( $bg_color ) ||
                !empty( $tit_color ) ||
                !empty( $desc_color ) ||
                !empty( $img_shadow ) ||
                !empty( $bg_img )
            )  : ?>

            <?php echo '<style>'; ?>

                <?php  /*Block Css*/ if (
                    !empty($bg_color) ||
                    !empty( $bg_img )
                ) : ?>
                <?php echo '.' . $uniq; ?> {
                    <?php echo $bg_color . $bg_img . $bg_fix . $bg_fill . $bg_rept . $bg_pos; ?>
                }
                <?php endif; ?>

                <?php /* Title CSS */ if (!empty($tit_color)) : ?>
                <?php echo '.' . $uniq . ' .hbk-title'; ?> {
                    <?php echo $tit_color; ?>
                }
                <?php endif; ?>

                <?php /* Description CSS */  if (!empty($desc_color)) : ?>
                <?php echo '.' . $uniq . ' .hbk-desc'; ?> {
                    <?php echo $desc_color; ?>;
                }
                <?php endif; ?>

                <?php /* Image CSS */ if (
                    in_array($hbk[block_img_align],array('out-left', 'out-right')) ||
                    (!empty($img_shadow))
                )  : ?>
                <?php echo '.' . $uniq . ' .hbk-img'; ?> {
                    <?php echo $img_shadow; ?>
                }
                <?php endif;?>

                <?php if (!empty($div_top)) : ?>
                <?php echo '.' . $uniq . ' .div-top'; ?> {
                    <?php echo 'fill:' . $hbk[block_bg_color] . ';'; ?>
                }
                <?php endif;?>

            <?php echo '</style>'; ?>
            <?php endif; // For Style tag ?>

            <?php if (!empty($div_top)) : ?>
                <?php echo $div_top; ?>
            <?php endif; ?>

            <div class="over-hide"><div class="container">

            <div class="cf py-5">
                <?php echo $row[open] = (in_array($hbk[block_img_align],array('out-left', 'out-right'))) ? '<div class="row' . $reverse = ($hbk[block_img_align] == 'out-right' ) ? ' flex-row-reverse">' : '' . '">': ''; ?>

                <?php if (!empty($hbk[block_image])) : ?>
                <div class="mb-5 hbk-img-wrap<?php echo $img_align . $anims[array_rand($anims,1)]; ?>">
                    <?php echo wp_get_attachment_image( $hbk[block_image], 'full', false, array( "class" => "hbk-img img-fluid" . $imgout_class . $img_style . $mw180 ) ); ?>
                </div>
                <?php endif;?>
                <?php echo $col[open] = (in_array($hbk[block_img_align],array('out-left', 'out-right'))) ? '<div class="col-md-6">': ''; ?>
                <?php if (!empty($hbk[block_title])) : ?>
                <h2 class="hbk-title txt-bold pb-4 h1<?php echo $tit_align . $anims[array_rand($anims,1)]; ?>"><?php echo $hbk[block_title]; ?></h2>
                <?php endif;?>

                <?php if (!empty($hbk[block_desc]) || !empty( $btn_txt ) ) : ?>
                <div class="hbk-desc<?php echo $desc_align . $anims[array_rand($anims,1)]; ?>">
                    <?php echo do_shortcode( $hbk[block_desc] );?>

                    <?php if ( !empty( $btn_txt ) ) : ?>
                        <p class="mt-3 txt-up"><a href="<?php echo $btn_link; ?>" class="btn px-md-5 px-3<?php echo $btn_color . $btn_style;?>"><?php echo $btn_txt; ?></a></p>
                    <?php endif; ?>

                </div>
                <?php endif;?>

                <?php echo $col[close] = (in_array($hbk[block_img_align],array('out-left', 'out-right'))) ? '</div></div>': ''; ?>

            </div></div>

            <?php echo $space; ?>

        </section>
    <?php endforeach; ?>

<?php endif; ?>
