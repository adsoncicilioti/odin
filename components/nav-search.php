<!-- Modal -->
<div class="modal modal-full fade" id="topSearch" tabindex="-1" role="dialog" aria-labelledby="topSearchTitle" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content bg-white-75">
			<div class="modal-header border-0">
				<button type="button" class="close p-1 rounded-0 bg-light" data-dismiss="modal" aria-label="Close">
					<span class="over-close rel d-block"></span>
				</button>
			</div>
			<div class="modal-body container d-flex align-items-center">
				<form method="get" class="col-12 col-lg-8 mx-auto mb-5" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
					<h3 class="h1 text-primary txt-bold txt-c mb-5"><?php _e( 'What are you looking for today?', 'odin' ); ?></h3>
					<div class="input-group">
						<input type="search" value="<?php echo get_search_query(); ?>" class="form-control " id="input-search" name="s" placeholder="<?php _e( 'Hi! Type here your search..', 'odin' ); ?>" required="required" autofocus/>

						<div class="input-group-append">
							<button type="submit" class="btn btn-primary txt-up sh shad-primary">
								<span class="d-none d-md-inline">
									<?php _e( 'Search', 'odin' ); ?>
								</span>
								<svg class="icon text-dark-blue h4 ml-1 mb-0"><use xlink:href="#search" /></svg>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
