<?php if ( function_exists( 'woocommerce_mini_cart' ) ) : ?>
  <input class="sr-only" type="checkbox" name="btn-toggle" id="toggle-minicart">
  <label class="btn-toggle toggle-cart text-center rel" name="btn-toggle" for="toggle-minicart">
	<?php if ( ! WC()->cart->is_empty() ) : ?>
	  <div class="abs badge badge-pill badge-danger z2 cart-has-item">!</div>
	<?php endif; ?>
	<svg class="icon">
	  <use xlink:href="#shop-cart" />
	</svg>
	<span class="sr-only"><?php esc_html_e( 'Quotation:', 'odin' ); ?></span>
  </label>

  <div id="nav_mini_cart" class="ease-bz bg-primary text-white abs-t-100 z-3 border-0 shad-primary invisible rounded-lg">
	<label class="close abs btn-danger rounded-lg" aria-label="Fechar" for="toggle-minicart">
	  <span aria-hidden="true">&times;</span>
	</label>
	<div class="p-3">
	  <?php woocommerce_mini_cart(); ?>
	</div>
  </div>
<?php endif; ?>
