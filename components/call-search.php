<?php
/** Component for call Modal search on another DOM location
 * Modal dialogis put on footer probaly
 */
?>

<!-- Button trigger modal -->
<button type="button" class="btn-toggle toggle-search no-bg" data-toggle="modal" data-target="#topSearch">
	<span class="sr-only"><?php esc_html_e( 'Search:', 'odin' ); ?></span>
	<svg class="icon text-white">
		<use xlink:href="#search" />
	</svg>
</button>
