<?php
/**
 * Call Sign
 *
 * Button for cal Modal Signup form
 *
 */

if ( is_user_logged_in() ) :
	?>
	<label class="p-2 mb-0 ml-1" data-toggle="modal" data-target="#signPup">
	<?php
		global $userdata;
		wp_get_current_user();
		echo get_avatar( $userdata->ID, 24, null, null, array( 'class' => array( 'rounded' ) ) );
	?>
	</label>
<?php else : ?>
	<label class="btn-toggle toggle-sign " data-toggle="modal" data-target="#signPup">
		<span class="sr-only"> <?php esc_html_e( 'User Profile', 'odin' ); ?></span>
		<svg class="icon text-primary">
			<use xlink:href="#user" />
		</svg>
	</label><!-- .sign-toggle -->
<?php endif; ?>
