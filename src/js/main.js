/**
 * Vendors
 */
// =include('../../node_modules/popper.js/dist/umd/popper.min.js');
// Required for Dropdowns and others

// Bootstrap
//=include('../../node_modules/bootstrap/js/dist/util.js');
// Required for Dropdowns and others (modals,..);

//=include('../../node_modules/bootstrap/js/dist/alert.js');
// =include('../../node_modules/bootstrap/js/dist/button.js');
// =include('../../node_modules/bootstrap/js/dist/carousel.js');
// =include('../../node_modules/bootstrap/js/dist/collapse.js');
// =include('../../node_modules/bootstrap/js/dist/dropdown.js');
//=include('../../node_modules/bootstrap/js/dist/modal.js');
// =include('../../node_modules/bootstrap/js/dist/scrollspy.js');
// =include('../../node_modules/bootstrap/js/dist/tab.js');
// =include('../../node_modules/bootstrap/js/dist/tooltip.js');
// =include('../../node_modules/bootstrap/js/dist/popover.js');

// Smoothscroll for Websites
// =include('../../node_modules/smoothscroll-for-websites/SmoothScroll.js');

// FitVids
//=include('../../node_modules/fitvids/dist/fitvids.js');

// Moment
//=include('../../node_modules/moment/min/moment.min.js');
//=include('../../node_modules/moment/locale/pt-br.js');

// Slick Carousel
// =include('../../node_modules/slick-carousel/slick/slick.js');

/**
 *	Main
 */

/* jshint ignore:start */

// var config = {
// 	viewFactor: 0.3,
// 	useDelay: 'always',
// 	duration: 800,
// 	delay: 200,
// 	distance: '250px',
// 	reset: false,
// 	scale: 0.99
// };

// window.sr = new ScrollReveal(config);

// sr.reveal('.sr-r', { origin: 'right' }, 100);
// sr.reveal('.sr-l', { origin: 'left' }, 100);
// sr.reveal('.sr-t', { origin: 'top' }, 100);
// sr.reveal('.sr-b', { origin: 'bottom' }, 100);
// sr.reveal('.sr-fade', { distance: '0px' }, 100);

// Code here will be ignored by JSHint.
/* jshint ignore:end */

jQuery(document).ready(function ($) {
	$("#topSearch").modal({
		backdrop: false,
		show: false,
	});

	$("#topSearch").on("shown.bs.modal", function () {
		$(this).find("[autofocus]").focus();
		$(".modal-blur").addClass("blur");
	});

	$("#topSearch").on("hide.bs.modal", function () {
		$(".modal-blur").removeClass("blur");
	});

	$("#signPup").on("shown.bs.modal", function () {
		$(this).find("[autofocus]").focus();
		$(".modal-blur").addClass("blur");
	});

	$("#signPup").on("hide.bs.modal", function () {
		$(".modal-blur").removeClass("blur");
	});

	/**
	 * Responsive wp_video_shortcode().
	 */

	$(".wp-video-shortcode").parent("div").css("width", "auto");

	/**
	 * Fluid width video embeds.
	 * @link http://fitvidsjs.com
	 */

	/**
	 * Relative time to date posts.
	 * @link http://momentjs.com
	 */

	moment.locale($("html").attr("lang"));

	$(".entry-date").each(function () {
		$(this).text(
			moment($(this).children("time").attr("datetime"), moment.ISO_8601)
				.startOf("hour")
				.fromNow()
		);
	});

	/**
	 * Odin Core shortcodes.
	 */

	// Tabs.
	//$( '.odin-tabs a' ).click(function(e) {
	//	e.preventDefault();
	//	$(this).tab( 'show' );
	//});

	// Tooltip.
	//$( '.odin-tooltip' ).tooltip();

	// alert
	$(".alert").alert();

	// Slick Carousel
	// $('.home-slideshow').slick({
	// 	autoplay: true,
	// 	autoplaySpeed: 8000
	// 	// pauseOnHover: false,
	// 	// pauseOnFocus: false
	// });
	// $('.slick-arrow')
	// 	.addClass('z3 d-flex')
	// 	.text('');
	// $('.slick-next').append(
	// 	'<svg class="icon"><use xlink:href="#chevron-r" /></svg>'
	// );
	// $('.slick-prev').append(
	// 	'<svg class="icon"><use xlink:href="#chevron-l" /></svg>'
	// );

	//////  ANCHOR SMOOTH
	// $('a[href^="#"]').click(function(e) {
	// 	e.preventDefault();
	// 	anchorScroll($(this), $($(this).attr('href')), 2400);
	// });

	// function anchorScroll(this_obj, that_obj, base_speed) {
	// 	var this_offset = this_obj.offset();
	// 	var that_offset = that_obj.offset();
	// 	var offset_diff = Math.abs(that_offset.top - this_offset.top);

	// 	var speed = (offset_diff * base_speed) / 1000;

	// 	$('html,body').animate(
	// 		{
	// 			scrollTop: that_offset.top
	// 		},
	// 		speed
	// 	);
	// }

	$('a[href*="#"]:not([href="#"])').click(function () {
		var target = $(this.hash);
		$("html,body")
			.stop()
			.animate(
				{
					scrollTop: target.offset().top - 0,
				},
				"linear"
			);
	});
	if (location.hash) {
		var id = $(location.hash);
	}
	$(window).load(function () {
		var offsetSize = $("#navbar-main-navigation").innerHeight();
		var adj = $(window).width() < 500 ? 6.25 : 2.75;
		if (location.hash) {
			$("html,body").animate(
				{ scrollTop: id.offset().top - offsetSize * adj },
				"linear"
			);
		}
	});

	$(' a[href*="traume"]')
		.not('[href*="/#"], [target="_blank"], [href^="mailto:"]')
		.click(function (e) {
			$("body").removeClass("loaded");
			e.preventDefault(); // prevent default anchor behavior
			var goTo = this.getAttribute("href"); // store anchor href
			// do something while timeOut ticks ...

			setTimeout(function () {
				window.location = goTo;
			}, 800);
		});

	/* Validation events for changing response CSS classes */

	document.addEventListener(
		"wpcf7invalid",
		function (event) {
			$(".wpcf7-response-output").addClass("alert-warning shadow");
		},
		false
	);
	document.addEventListener(
		"wpcf7spam",
		function (event) {
			$(".wpcf7-response-output").addClass("alert-warning shadow");
		},
		false
	);
	document.addEventListener(
		"wpcf7mailfailed",
		function (event) {
			$(".wpcf7-response-output").addClass("alert-danger shadow");
		},
		false
	);
	document.addEventListener(
		"wpcf7mailsent",
		function (event) {
			$(".wpcf7-response-output").addClass("alert-success shadow");
		},
		false
	);

	// Custom File Input
	$(document).on("change", ".custom-file-input", function () {
		var fileName = $(this).val().split("\\").pop();
		$(this).closest(".custom-file").find(".custom-file-label").html(fileName);
	});
});

// FitVids Call
fitvids();
