<?php
/**
 * The template for displaying Category pages.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Odin
 * @since 2.2.0
 * ADSON
 */

get_header();

// start of Content and Wrappers
get_template_part( 'components/content', 'start' );
if ( have_posts() ) :

	// Start the Loop.
	while ( have_posts() ) :
		the_post();

		/*
		* Include the post format-specific template for the content. If you want to
		* use this in a child theme, then include a file called called content-___.php
		* (where ___ is the post format) and that will be used instead.
		*/
		get_template_part( 'components/content', get_post_format() );

	endwhile;

	// Page navigation.
	odin_paging_nav();

	else :
		// If no content, include the "No posts found" template.
		get_template_part( 'components/content', 'none' );

endif;

	// end of Content and Wrappers
	get_template_part( 'components/content', 'end' );

